logistic.dma.predict <-
function (x,ldma, inds=NULL ) {
# function to get predicted values for a set of 1 or more x's
  require(Matrix)

  #if( !weighted ) {
  if( is.null(inds) ) {
    return( Matrix::crossprod( x, ldma$theta ) ) 
  } else {
    return( Matrix::crossprod( x, ldma$theta[,inds] ) ) 
  }
  #} else {
  #  M <- Matrix::crossprod( x,ldma$theta )
  #  M[M>22.5] <- 22.5
  #  M <- exp(M)
  #  return( (M/(1.0+M)) %*% ldma$pi.t.t )
  #}
}

