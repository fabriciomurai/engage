#options(error=recover)

GETRESULT=TRUE
SAVEOUTPUT=TRUE
COUNTFEATS=TRUE
GETTIMES=TRUE
NRUNS=100

args <- commandArgs(trailingOnly = TRUE)
if( length(args) >= 2 ) {
  dataset = tolower(args[1])
  method = tolower(args[2])
  bname <- sprintf("%s/%s-seeAll-budget.minocc2.r",dataset,method)

  if( length(args) == 3 )
    NRUNS = as.numeric(args[3])
} else {
  stop('Invalid arguments. Try:\
\tRscript snapshot_inspector2.R <dataset> <method> [<nruns>]\
where:
\t<dataset> is the dataset name as in problem.R\
\t<method> is one of naive-ones mrmb-ones mab-ones-par rforest-ones\
\t<nruns> specifies that statistics should be calculated over 1:nruns')
}

is_nomab <- length(grep("nomab",method))>0
is_mabpar <- length(grep("mab-.*-par",method))>0
is_naive <- length(grep("naive",method))>0
is_mrmb <- length(grep("mrmb",method))>0
is_rforest <- length(grep("rforest",method))>0


list_npositive <- list()
list_nfeats <- list()
list_times <- c()
nsamples <- c()
notfound = NULL
for( run_id in  1:NRUNS ) {
  basename <- sprintf("%s%03d", bname, run_id)
  tmp_filename <-sprintf("%s.%03d.RData",basename,0)
  if( !file.exists(tmp_filename) ) {
    notfound=tmp_filename
      next
  }

  if( !is.null(notfound) ) {
    print(paste('Attention! File not found:', notfound))
    notfound = NULL
  }

  if( GETRESULT ) {
    i = 100
    new_filename=sprintf("%s.%03d.RData",basename,i)
    while( file.exists(new_filename) ) {
       filename=new_filename
       i = i+100
       new_filename=sprintf("%s.%03d.RData",basename,i)
    }
    nsamples <- c(nsamples,i-100)
    load(filename)
    list_npositive <- append(list_npositive,list(cumsum(snapshot$y)))
  }
  if( GETTIMES ) {
    i = 0
    new_filename=sprintf("%s.%03d.RData",basename,i)
    initial_time <- NULL
    last_time <- NULL
    while( file.exists(new_filename)  && i<= 1000) {
       filename=new_filename
       timestamp <- file.info(new_filename)$ctime
       if( is.null(initial_time) )
           initial_time <- timestamp
       last_time <- timestamp
       i = i+100
       new_filename=sprintf("%s.%03d.RData",basename,i)
    }
    nsamples <- c(nsamples,i-100)
    if( !is.null(last_time) && !is.null(initial_time) && last_time > initial_time)
        list_times <- c(list_times,as.numeric(last_time-initial_time,units="secs"))
  }

  if( COUNTFEATS ) {
    hist_nfeats <- c()
    i = 100
    new_filename=sprintf("%s.%03d.RData",basename,i)
    while( file.exists(new_filename) && i <= 1000 ) {
       filename=new_filename
       i = i+100
       new_filename=sprintf("%s.%03d.RData",basename,i)
    }
    load(filename)

    i = i-100
    if( is_mabpar ) {
      list_nfeats <- append(list_nfeats,list(snapshot$hist_nfeats))
    } else {
      admtimes <- snapshot$admission_times
      nfeats <- table(admtimes[which(admtimes>0)])
      times <- as.numeric(names(nfeats))
      hist_nfeats <- rep(0,i)
      hist_nfeats[times] <- hist_nfeats[times]+nfeats
      list_nfeats <- append(list_nfeats,list(cumsum(hist_nfeats)))
    }
  }
}

if( GETRESULT ) {
  max_samples = max(nsamples)
  if( any(nsamples < max_samples) ) {
    print('Attention! Some runs have less samples:')
    affected_runs = which(nsamples!=max_samples)
    print(cbind(affected_runs,nsamples[affected_runs]))
  }

  if( SAVEOUTPUT ) {
      outfile=sprintf("%s/npositive_%s.RData",dataset,method)
      save(list_npositive,file=outfile)
      print(paste('Output saved to', outfile))
    }

  a <- c()
  for( b in list_npositive )
    if( length(b) >= max_samples )
  a <- c(a,b[max_samples])
  cat("Mean:",mean(a,na.rm=TRUE)/max_samples,"\n")
  cat("Median:",median(a,na.rm=TRUE)/max_samples,"\n")
  cat("Sorted:",sort(a),"\n")
}

if( GETTIMES ) {
  print(sort(list_times))
  print(c(mean(list_times),median(list_times)))
  if( SAVEOUTPUT ) {
      outfile=sprintf("%s/times_%s.RData",dataset,method)
      save(list_times,file=outfile)
      print(paste('Output saved to', outfile))
  }
}

if( COUNTFEATS && SAVEOUTPUT ) {
  outfile=sprintf("%s/nfeats_%s.RData",dataset,method)
  #print(list_nfeats)
  save(list_nfeats,file=outfile)
  print(paste('Output saved to', outfile))
}
