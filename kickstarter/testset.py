import csv
import sys
import gzip
from collections import defaultdict, deque

def possibleTestsets():
    csv.field_size_limit(sys.maxsize)
    backers = []
    # store susceptible backers prior to 2013
    with open('susceptible_backers.txt') as tsvin:
        tsvin = csv.reader(tsvin,delimiter='\t')
        for line in tsvin:
            backers += line

    backers = frozenset(backers)
    print len(backers)
    sys.exit(0)

    # count how many susceptible backers reappear in 2013 projects
    counts = []
    with open('test_backers.txt') as tsvin:
        tsvin = csv.reader(tsvin,delimiter='\t')
        for line in tsvin:
            tmp = len(backers.intersection(line))
            counts.append( (tmp*1.0/len(line),tmp,len(line), len(counts)) )

    counts.sort(reverse=True)
    print 'fraction\tabsolute\t|backers|\tproject_newid'
    for t in counts:
        print '{:.2}\t{}\t{}\t{}'.format(t[0],t[1],t[2],t[3])

def gen_graph():
    ungraphfile = 'kickstarter.ungraph.txt.gz'

    print 'Reading cliques'
    graph = defaultdict(set)
    with open('susceptible_backers.txt') as tsvin:
        tsvin = csv.reader(tsvin,delimiter='\t')
        for i,line in enumerate(tsvin):
            ids = map(int,line)
            for idx in ids:
                for jdx in ids:
                    if idx != jdx:
                        graph[idx].add(jdx)
                        graph[jdx].add(idx)

    # run BFS from node 48
    marked = {v:False for v in graph}
    queue = deque()
    seed = 5120
    queue.append(seed)
    marked[seed] = True

    while len(queue) > 0:
        v = queue.pop()
        for u in graph[v]:
            if not marked[u]:
                queue.append(u)
                marked[u] = True

    print 'Saving graph'
    with gzip.open(ungraphfile,'w') as tsvout:
        tsvout = csv.writer(tsvout,delimiter='\t')
        for v in graph:
            if marked[v]:
                for u in graph[v]:
                    if v < u:
                        tsvout.writerow([v,u])

if __name__ == "__main__":
    gen_graph()
