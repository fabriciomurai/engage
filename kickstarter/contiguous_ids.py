import fileinput
import csv
import gzip

attribs_file='susceptible_backers.txt'
new_attribs_file='kickstarter.attribs.txt.gz'
targets_file='test_backers.txt'
target_id=7793

graph_file='kickstarter.adj.gz'
new_graph_file='kickstarter.ungraph.txt.gz'

old2new = dict()

with open(attribs_file,'r') as tsvin, open(targets_file,'r') as tsvin2, gzip.open(new_attribs_file,'w') as tsvout:
    tsvin = csv.reader(tsvin,delimiter='\t')
    tsvin2 = csv.reader(tsvin2,delimiter='\t')
    tsvout = csv.writer(tsvout,delimiter='\t')
    print 'Processing attributes file'
    for line in tsvin:
        ids = map(int,line)
        for oldid in ids:
            if oldid not in old2new:
                old2new[oldid] = len(old2new)
        tsvout.writerow([old2new[oldid] for oldid in ids])
    print 'Processing targets file'
    for idx,line in enumerate(tsvin2):
        if idx == target_id:
            ids = map(int,line)
            tsvout.writerow([old2new[oldid] for oldid in ids if oldid in old2new])

maxid_attrib = len(old2new)
maxid_graph = -1

with gzip.open(graph_file,'r') as tsvin, gzip.open(new_graph_file,'w') as tsvout:
    tsvin = csv.reader(tsvin,delimiter='\t')
    tsvout = csv.writer(tsvout,delimiter='\t')
    print 'Processing graph file'
    for line in tsvin:
        ids = map(int,line)
        for oldid in ids:
            if oldid not in old2new:
                old2new[oldid] = len(old2new)
        newids=[old2new[oldid] for oldid in ids]
        tsvout.writerow(newids)
        maxid_graph = max(newids+[maxid_graph])

    print 'max id from attrib file:', maxid_attrib, '\nmax id from graph file:', maxid_graph
    if maxid_attrib > maxid_graph:
        tsvout.writerow([maxid_attrib+1,maxid_attrib+2])
