import csv
import gzip
import json
from collections import defaultdict

attribs_file = 'com-amazon.top5000.cmty.txt.gz'
ungraph_file = 'com-amazon.ungraph.txt.gz'
target_trait = 4832
id2outedges = defaultdict(list)
id2attribs = defaultdict(list)

with gzip.open(ungraph_file,'r') as tsvin:
    tsvin = csv.reader(tsvin,delimiter='\t')
    for line in tsvin:
        if not line[0].startswith('#'):
            ids = map(int,line)
            id2outedges[ids[0]].append(ids[1])

with gzip.open(attribs_file,'r') as tsvin:
    tsvin = csv.reader(tsvin,delimiter='\t')
    for idx,line in enumerate(tsvin):
        ids = map(int,line)
        for nodeid in ids:
            id2attribs[nodeid].append(idx)

for node in frozenset(id2outedges.keys()+id2attribs.keys()):
    print json.dumps( {'_id': node, 'lab': 4832 in id2attribs[node],'nbr' : id2outedges[node], 'att' : id2attribs[node] } )

# Usage:
#     python engage/backend/populateMongoDB.py > amazon.json
#     mongoimport --upsert --db amazon --collection nodes --file amazon.json
