AdaLaunch Project (Backend and Website)
=======================

* Cesar Marcondes (UFSCar)
* Bruno Ribeiro (CMU)
* Fabricio Murai (UMass)

How to setup the backend and website

1. On the UFSCar virtual machines, it was installed apache, python and rabbitMQ

* apache2 (used at UFSCar) is not using cgi, thus enable it
```
cesar@sin2 ~> sudo a2enmod cgi
[sudo] password for cesar:
Your MPM seems to be threaded. Selecting cgid instead of cgi.
Enabling module cgid.
To activate the new configuration, you need to run:
  service apache2 restart

cesar@sin2 ~> sudo service apache2 restart
 * Restarting web server apache2                                                                [ OK ]
```

* install bulbs (necessary for cgi python processing neo4j)
```
cesar@sin2 /var/log/apache2> sudo easy_install bulbs
Searching for bulbs
Reading https://pypi.python.org/simple/bulbs/
Best match: bulbs 0.3.29-20140417
Downloading https://pypi.python.org/packages/source/b/bulbs/bulbs-0.3.29-20140417.tar.gz#md5=75d74e8f1fd51e5399f31f1ac89b095e
...
Installed /usr/local/lib/python2.7/dist-packages/distribute-0.7.3-py2.7.egg
Finished processing dependencies for bulbs
```

* install RabbitMQ server (assume Debian)
```
cesar@sin2 ~> sudo vim /etc/apt/sources.list
(add the following line to add the rabbitmq repository)
deb http://www.rabbitmq.com/debian/ testing main

cesar@sin2 ~> wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
...
2014-09-24 15:30:39 (108 MB/s) - “rabbitmq-signing-key-public.asc” salvo [1702/1702]

cesar@sin2 ~> sudo apt-key add rabbitmq-signing-key-public.asc
OK

cesar@sin2 ~> sudo apt-get update
(to incorporate the new repository)

cesar@sin2 ~> sudo apt-get install rabbitmq-server
...
Configurando rabbitmq-server (3.3.5-1) ...
Adicionando grupo `rabbitmq' (GID 117) ...
Concluído.
Adicionando usuário de sistema 'rabbitmq' (UID 107) ...
Adicionando novo usuário `rabbitmq' (UID 107) ao grupo `rabbitmq' ...
Sem criar diretório pessoal `/var/lib/rabbitmq'.
 * Starting message broker rabbitmq-server                                                      [ OK ]
Processing triggers for libc-bin (2.19-0ubuntu6.3) ...
Processing triggers for ureadahead (0.100.0-16) ...
```

* TODO: replicate steps in AWS cloud

2. Download the project code
* git clone git@bitbucket.org:fabriciomurai/engage.git

3. Facebook application configuration
* TODO: document this part
