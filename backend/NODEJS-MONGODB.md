npm install mongodb -g
cd /path/to/my/app/folder
npm link mongodb

(run node.js as a daemon)
npm install forever
usage: forever [action] [options] SCRIPT [script-options]
Monitors the script specified in the current process or as a daemon

npm install express
cd /path/to/my/app/folder
npm link express
