import cgi
import json
#import numpy as np
from random import shuffle
#from neo4jrestclient.client import GraphDatabase
import pika
from os.path import isfile
import pickle
from pymongo import MongoClient
from collections import defaultdict

def getInfo( node_ids ):
    if len(node_ids) > 0:
        return list(collection.find({ '_id' : { '$in' : node_ids } }))
    else:
        return []

#def loadModel():
#    if isfile(modelfile):
#        beta = pickle.load(modelfile)
#    else:
#        beta = np.arange(10)

    return beta

def scoreNodes( feature_vals ):
    # TODO: use beta
    scores = range(0,len(feature_vals))
    return scores

def as_node(dct):
    label = []
    if 'label' in dct:
        label = dct['label']

    feats = []
    if 'feats' in dct:
        feats = dct['feats']

    return dct['_id'], label, dct['att'], dct['nbr'], feats

def updateFeatures( label, attribs, neighs, friends_info ):
    touched_ids = []
    feature_vals = []
    for friend in friends_info:
        touched_ids.append(friend['_id'])
        if 'feats' not in friend:
            feats = defaultdict(float)

        # feature update in what follows
        feats[0] += label
        feats[1] = feats[0]/max(1,len(friend['nbr']))
        feature_vals.append(feats)

    return touched_ids, feature_vals

from pprint import pprint

def updateGraph( touched_ids, feature_vals, seed, friends_info ):
    bulk = collection.initialize_ordered_bulk_op()
    for idx,nodeid in enumerate(touched_ids):
        bulk.find({'_id':nodeid}).upsert().update({'$set':{'fkeys': feature_vals[idx].keys(), 'fvals' : feature_vals[idx].values()}})
    bulk.find({'_id':seedid}).upsert().update({'$set':seed})
    result = bulk.execute()
    pprint(result)


# mongodb connection
hostname = 'localhost'
port=27017
#port=27232 # (pantanal)
client = MongoClient(hostname,port)
db = client['amazon']
collection = db['nodes']

# input example
# json_obj = '{"nodeid":2,"label":1,"attribs":[1,10,100],"neighs":[1,3,4]}'
beta = []
modelfile = 'current_model.pkl'
seedid = 79870
seed    = collection.find_one({ '_id' : seedid })
attribs = seed['att']
neighs  = seed['nbr']
label   = seed['lab']


## parse message
## FIXME: obtain message from cgi form
##form = cgi.FieldStorage()
##message = form.getValue("message", "{}" )
#message = json_obj
#nodeid, label, attribs, neighs, feats = as_node(json.loads(message))

friends_info = getInfo( neighs )

# TODO: compute features (in parallel?) -- blocking call
touched_ids, feature_vals = updateFeatures( 1*label, attribs, neighs, friends_info )
scores = scoreNodes( feature_vals )
ranking = [neighid for _,neighid in sorted(zip(scores,touched_ids), reverse=True) ]

# TODO: create thread to update graph
updateGraph( touched_ids, feature_vals, seed, friends_info )

# TODO: return the ranking to Facebook App

# TODO: send information to be added to the graph by another process
#connection = pika.BlockingConnection(pika.ConnectionParameters(
#        host=hostname))
#channel = connection.channel()
#channel.queue_declare(queue='newNode')
#channel.basic_publish(exchange='',
#                      routing_key='newNode',
#                      body=json_obj)
#connection.close()
