import pika
import pyRserve
import json
import numpy as np
import pickle
from os.path import isfile


hostname = 'localhost'
modelfile = 'current_model.pkl'

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=hostname))
inchannel = connection.channel()
inchannel.queue_declare(queue='newNode')

conn = pyRserve.connect()

def rinit():
    conn.eval('print("TODO: Load previous observations, correlation values and current coefficients")')
    conn.r.x = np.empty([0,10])
    conn.r.y = np.empty([0])
    conn.r.rho = np.zeros(10)
    if isfile(modelfile):
        conn.r.beta = pickle.load(modelfile)

    else:
        conn.r.beta = np.arange(10)

def as_observation(body):
    x = np.arange(10)*1.1
    y = 1
    return x, y

def callback(ch, method, properties, body):
    print ch, method, properties, body
    obs_x, obs_y = as_observation(body)
    conn.r.x = np.vstack((conn.r.x,obs_x))
    conn.r.y = np.append(conn.r.y,[obs_y])
    # TODO: recalculate beta and publish new model only if queue is empty
    conn.r.beta *= 2
    with open(modelfile,'wb') as output:
        pickle.dump(conn.r.beta,output)


rinit()
inchannel.basic_consume(callback,
                      queue='newNode',
                      no_ack=True)
inchannel.start_consuming()
