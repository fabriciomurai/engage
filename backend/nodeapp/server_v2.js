var express = require('express');
var https = require('https');
var http = require('http');
var fs = require('fs');

// This line is from the Node.js HTTPS documentation.
var options = {
    key: fs.readFileSync('/var/www/nodeapp/cert/private.pem'),
      cert: fs.readFileSync('/var/www/nodeapp/cert/public.pem')
};

var app = express();

// static content is served
app.use('/', express.static(__dirname + '/static'));

http.createServer(app).listen(80);
https.createServer(options, app).listen(443);
