var express = require('express');
var bodyParser = require('body-parser')
var https = require('https');
var http = require('http');
var fs = require('fs');

// This line is from the Node.js HTTPS documentation.
var options = {
    key: fs.readFileSync('/var/www/nodeapp/cert/private.pem'),
      cert: fs.readFileSync('/var/www/nodeapp/cert/public.pem')
};

var app = express();

// static content is served
app.use('/', express.static(__dirname + '/static'));
http.createServer(app).listen(80);
https.createServer(options, app).listen(443);

// dynamic content using postgraph
app.use(bodyParser.json());

app.post('/postgraph', function(request, response){
  console.log(request);
  console.log(request.body);      // your JSON
  response.send(request.body);    // echo the result back
});
