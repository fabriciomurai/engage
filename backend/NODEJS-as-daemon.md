# STEPS

## INSTALL NODE.JS

sudo apt-get update
sudo apt-get install build-essential libssl-dev
sudo apt-get install nodejs npm
sudo apt-get install nodejs-legacy

## Running a Node.js process on Debian as an init.d Service
sudo npm install -g forever
(-g option is a global configuration, thus accessible to any node.js app)

sudo mkdir /var/log/forever
(log files for forever apps running)

sudo mkdir /var/www/nodeapp
sudo npm link forever
(store your node.js project in some appropriate directory)

# Configuring the init.d Service
sudo touch /etc/init.d/nodeapp
sudo chmod a+x /etc/init.d/nodeapp
sudo update-rc.d nodeapp defaults

- copy the nodeapp SERVICE file, configure (vim) and fire on and off
sudo service nodeapp start
sudo service nodeapp stop

- add the appropriate symbolic links to cause the script 
- to be executed when the system goes down, or comes up
sudo update-rc.d nodeapp defaults


