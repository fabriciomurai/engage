#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys

# enable debugging
import cgitb
cgitb.enable()

print "Content-Type: application/j-son;charset=utf-8"
print

f = open("/var/www/html/data/graph2.json","r")

for line in f.xreadlines():
	sys.stdout.write(line)

f.close()
