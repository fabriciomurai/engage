CXXSRCDIR=.
CXXINCLUDE=-I${CXXSRCDIR}
CXXOBJDIR=build/objects/cc
CXXOPTS=-fpermissive -fkeep-inline-functions -std=c++11 -arch x86_64 -lstdc++
CXXDEBUG=-g -ggdb
CXX=g++

CXXCOMPILE=${CXX} ${CXXDEBUG} ${CPPFLAGS} ${CXXOPTS} ${CXXINCLUDE}
#CXXCOMPILE=${CXX} ${CPPFLAGS} ${CXXOPTS} ${CXXINCLUDE}

all: pnb

pnb: pnb.cpp
	${CXXCOMPILE} -o $@ $< RandomLib/Random.cpp -I. -lgsl -lgslcblas -lpthread -L/opt/local/lib -I/opt/local/include
