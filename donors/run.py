import projects
import donations
from datetime import datetime

p2s,s2p,pd = projects.projects_by_school()
dc = donations.donations_per_school( p2s )
school_id = max(dc,key=lambda k:dc[k])

project_dates  = [pd[p] for p in s2p[school_id]]
trainset = frozenset([p for p in s2p[school_id]  if pd[p] < datetime(2013,1,1) ])
testset  = frozenset([p for p in s2p[school_id]  if  datetime(2013,1,1) <= pd[p] < datetime(2014,1,1) ])

p2d,targets,amounts = donations.gen_graph(trainset,testset)
