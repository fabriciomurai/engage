// -*- mode: C++; c-indent-level: 4; c-basic-offset: 4; indent-tabs-mode: nil; -*-
//
// rcpp_module.cpp: Rcpp R/C++ interface class library -- Rcpp Module examples
//
// Copyright (C) 2010 - 2012  Dirk Eddelbuettel and Romain Francois
//
// This file is part of Rcpp.
//
// Rcpp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// Rcpp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rcpp.  If not, see <http://www.gnu.org/licenses/>.

#include <Rcpp.h>
#include <RcppEigen.h>
#include <stdio.h>
#include <unordered_set>
#include <fstream>
#include <queue>
#include <algorithm>
#include <map>
#define MAX( a, b ) ( (a) > (b) ? (a) : (b) )
#define MIN( a, b ) ( (a) < (b) ? (a) : (b) )

#define DEBUG 0
#define INCLUDE_RW 1
#define RECALCULATE_RECNODE_FEATS 1
using namespace Rcpp;

typedef Eigen::SparseMatrix<double> SpMat; 
typedef std::pair<double,int> ScoreModelPair;
typedef std::map<int,double> NodeScoreMap;
typedef std::pair<int,double> SpEntry;
typedef std::vector<SpEntry> MySpVec;


bool compareModels ( const ScoreModelPair& l, const ScoreModelPair& r) {
   if( l.first < r.first ) return 1;
   else if( l.first > r.first ) return 0;
   else return l.second > r.second;
 }
bool compareSpEntry ( const SpEntry& l, const SpEntry& r)
{ return l.first < r.first; }


class DynamicIndex {
    private:
        std::map<int,int> id2index;
        std::queue<int> free_inds;
    public:
        std::map<int,int>& getMap( void ){ return id2index; };
        int operator[](std::size_t idx) {
            std::map<int,int>::iterator it = id2index.find(idx);
            int ind;
            if( it ==  id2index.end() ) {
                if( !free_inds.empty() ) {
                    ind = free_inds.front();
                    free_inds.pop();
                    id2index[idx] = ind;
                } else {
                    ind = id2index.size();
                    id2index[idx] = ind;
                }
            } else {
                ind = it->second;
            }
            return ind;
        }

        std::map<int,int>::iterator find( int idx ) { return id2index.find(idx); }
        void erase( int idx ) { free_inds.push( id2index[idx] ); id2index.erase(idx); };
        std::size_t size() { return id2index.size(); };
        bool empty() { return id2index.empty(); }
        std::map<int,int>::iterator end() { return id2index.end(); }
};



class MySpMat {
    public:
        // creates empty matrix
        MySpMat( void ) { actual_size = 0; };
        // creates empty matrix, ncols is known
        MySpMat( int ncols_ ) : ncols(ncols_) { x.resize(1000); actual_size = 0; }
        // creates empty matrix, ncols is known, initial nrows is given
        MySpMat( int nrows_, int ncols_ ) : ncols(ncols_) { x.resize(nrows_); actual_size = 0; }

        int rows() { return actual_size; }
        int cols() { return ncols; }

        MySpMat( IntegerMatrix *mmat, List betahat, bool intercept ) {
            int offset = 0;
            if( intercept ) offset = 1;

            ncols = mmat->nrow()+offset;
            x.resize(betahat.size());
            actual_size = betahat.size();

            for( int j = 0; j < betahat.size(); j++ ) {
                //printf("model_ind = %d\n", j );
                NumericVector model_coefs = wrap(betahat(j));
                MySpVec coefs_sparse;
                int pos = 0;

                if( intercept )
                    coefs_sparse.push_back(std::make_pair(0,model_coefs[pos++])); // intercept

                for( int i = 0; i < ncols; i++ ) {
                    if( (*mmat)(i,j) == 1 ) {
                        //printf("feat[%d] = %lf\n", i, model_coefs[pos] );
                        coefs_sparse.push_back(std::make_pair(i+offset,model_coefs[pos++]));
                    }
                }
                insert( coefs_sparse, j );
            }
        }

        void setCols( int ncols_ ) { ncols = ncols_; }

        // insert new row
        void insert( NumericVector dense, int ind );
        void insert( std::vector<double> dense, int ind );
        void insert( std::vector<double> dense, int ind, double intercept );
        void insert( MySpVec sparse, int ind );

        // multiply by numeric matrix
        NumericMatrix operator*( NumericMatrix coef );
        void fastMultiply( NumericMatrix coef, std::map<int,int> &id2index, std::map<int,double> &score );
        void fastMultiply( NumericMatrix coef, std::map<int,int> &id2index, std::vector<std::map<int,double> > &score );
        void fastMultiply( MySpMat coef, std::map<int,int> &id2index, std::vector<std::map<int,double> > &score );

        // get row
        std::vector<double> operator[](std::size_t idx) {
          std::vector<double> tmp(ncols);
            for( MySpVec::iterator it = x[idx].begin(); it != x[idx].end(); ++it )
                tmp[it->first] = it->second;
            return tmp;
        }

        void print() {
            for( int idx = 0; idx < actual_size; idx++ ) {
                printf("[%2d]", idx+1 );
                for( MySpVec::iterator it = x[idx].begin(); it != x[idx].end(); ++it )
                    printf( " %d:%.2lf", it->first+1, it->second );
                printf("\n");
            }
        }

        // erase row data
        void erase( int ind ) {
            x[ind].clear();
        }
        int size() { return actual_size; }
        // get specific element
        double get( int row, int ind ) {
          MySpVec::iterator it = std::lower_bound(x[row].begin(),x[row].end(),std::make_pair(ind, 0.0),compareSpEntry);
          if( it == x[row].end() || it->first != ind ) // all entries < ind OR entry not found
            return 0.0;
          else
            return it->second;
        }
        MySpVec &get( int row ) {
            return x[row];
        }
        MySpVec copy( int row ) {
            return x[row];
        }
    private:
        void checkCols( int rows ) {
            if( ncols != rows ) {
                char err_msg[100];
                sprintf( err_msg, "MySpMat::operator* -- operands have different sizes: expected is %d and argument is %d\n", ncols,rows  );
                throw std::runtime_error( err_msg );
            }
        }
        void checkRows( int ind ) {
            int len = x.size();
            if(ind > len-1) {
                x.resize( MAX(ind+1,MAX((int)(len*1.1),len+500)) );
            }
            actual_size = MAX(actual_size,ind+1);
        }
        std::vector< MySpVec > x;
        int ncols;
        int actual_size;
};

void MySpMat::insert( NumericVector dense, int ind ) {
    checkCols(dense.size());
    checkRows(ind);
    x[ind].clear();
    for( int i = 0; i < dense.size(); i++ )
        if( dense[i] != 0 )
            x[ind].push_back( std::make_pair(i,dense[i]) );
}
void MySpMat::insert( std::vector<double> dense, int ind ) {
    checkCols(dense.size());
    checkRows(ind);
    x[ind].clear();
    for( int i = 0; i < dense.size(); i++ ) {
        if( dense[i] != 0.0 )
            x[ind].push_back( std::make_pair(i,dense[i]) );
    }
}
void MySpMat::insert( std::vector<double> dense, int ind, double intercept ) {
    checkCols(dense.size()+1);
    checkRows(ind);
    x[ind].clear();
    x[ind].push_back(std::make_pair(0,intercept));
    for( int i = 0; i < dense.size(); i++ )
        if( dense[i] != 0.0 )
            x[ind].push_back( std::make_pair(i+1,dense[i]) );
}
void MySpMat::insert( MySpVec sparse, int ind ) {
    checkRows(ind);
    x[ind] = sparse;
}

NumericMatrix MySpMat::operator*( NumericMatrix coef ) {
    checkCols(coef.rows());
    NumericMatrix ret(actual_size,coef.cols());
    for( int j = 0; j < coef.cols(); j++ )
        for( int i = 0; i < actual_size; i++ ) {
            double sum = 0.0;
            for( MySpVec::iterator it=x[i].begin(); it != x[i].end(); ++it )
                sum += coef[it->first] * it->second;
            ret(i,j) = sum;
        }
    return ret;
}

void MySpMat::fastMultiply( NumericMatrix coef, std::map<int,int> &id2index, std::map<int,double> &score ) {
    checkCols(coef.rows());
    for( std::map<int,int>::iterator id_it=id2index.begin(); id_it != id2index.end(); ++id_it ) { //2nd is index, 1st is id
        double sum = 0.0;
        for( MySpVec::iterator it=x[id_it->second].begin(); it != x[id_it->second].end(); ++it ) //1st is col, 2nd is value
            sum += coef[it->first] * it->second;
        score[id_it->first] = sum;
    }
}

void MySpMat::fastMultiply( NumericMatrix coef, std::map<int,int> &id2index, std::vector<std::map<int,double> > &score ) {
    checkCols(coef.rows());
    for( int j = 0; j < coef.cols(); j++ ) {
        for( std::map<int,int>::iterator id_it=id2index.begin(); id_it != id2index.end(); ++id_it ) { //2nd is index, 1st is id
            double sum = 0.0;
            for( MySpVec::iterator it=x[id_it->second].begin(); it != x[id_it->second].end(); ++it ) //1st is col, 2nd is value
                sum += coef(it->first,j) * it->second;
            score[j][id_it->first] = sum;
        }
    }
}
void MySpMat::fastMultiply( MySpMat coef_, std::map<int,int> &id2index, std::vector<std::map<int,double> > &score ) {
    checkCols(coef_.cols());
    for( int i = 0; i < coef_.rows(); i++ ) {
        MySpVec coef = coef_.get(i);
        for( std::map<int,int>::iterator id_it=id2index.begin(); id_it != id2index.end(); ++id_it ) { //2nd is index, 1st is id
            MySpVec::iterator coef_it = coef.begin();
            MySpVec::iterator feat_it = x[id_it->second].begin();
            double sum = 0.0;
            while( coef_it != coef.end() && feat_it != x[id_it->second].end() ) {
                if( coef_it->first < feat_it->first ) {
                    coef_it++;
                } else if( coef_it->first > feat_it->first ) {
                    feat_it++;
                } else {
                    sum += coef_it->second * feat_it->second;
                    coef_it++; feat_it++;
                }
            }
            score[i][id_it->first] = sum;
        }
    }
}

class RunningStat
{
    public:
        RunningStat() : m_n(0) {}

        void Clear() {
            m_n = 0;
        }

        void Push(double x) {
            m_n++;
            // See Knuth TAOCP vol 2, 3rd edition, page 232
            if (m_n == 1) {
                m_oldM = m_newM = x;
                m_oldS = 0.0;
            } else {
                m_newM = m_oldM + (x - m_oldM)/m_n;
                m_newS = m_oldS + (x - m_oldM)*(x - m_newM);
    
                // set up for next iteration
                m_oldM = m_newM; 
                m_oldS = m_newS;
            }
        }

        int NumDataValues() const {
            return m_n;
        }

        double Mean() const {
            return (m_n > 0) ? m_newM : 0.0;
        }

        double Variance() const {
            return ( (m_n > 1) ? m_newS/(m_n - 1) : 0.0 );
        }

        double StandardDeviation() const {
            return sqrt( Variance() );
        }

    private:
        int m_n;
        double m_oldM, m_newM, m_oldS, m_newS;
};

class RunningCorrelation {
    public:
        RunningCorrelation() : m_n(0), sum_xy_minus_xbarybar(0.0), sum_xy(0.0) {}

        void Push( double x_, double y_ ) {
            m_n++;
            sum_xy += x_*y_;
            //if( m_n > 1 ) // when m_n == 1, sum is zero
            //    sum_xy_minus_xbarybar = sum_xy_minus_xbarybar + x_*y_ - x.Mean()*y_ - y.Mean()*x_ + x.Mean()*y.Mean();
            x.Push(x_);
            y.Push(y_);
        }
        void Clear() {
            sum_xy_minus_xbarybar = 0.0;
            sum_xy = 0.0;
            x.Clear();
            y.Clear();
            m_n = 0;
        }
        double Correlation() const {
            double x_sdev = x.StandardDeviation();
            double y_sdev = y.StandardDeviation();
            //return ( (m_n > 1 && x_sdev > 0 && y_sdev > 0) ? sum_xy_minus_xbarybar/((m_n-1) * x_sdev * y_sdev ) : 0.0 );
            return ( (m_n > 1 && x_sdev > 0 && y_sdev > 0) ? MIN(1.0,MAX(-1.0,(sum_xy-m_n*x.Mean()*y.Mean())/((m_n-1) * x_sdev * y_sdev ))) : 0.0 );
        }
        void PrintSum() const {
            fprintf( stderr, "sum:%e\n", sum_xy_minus_xbarybar);
        }
        void CorrelationWithBounds( double &r, double &conf_lb, double &conf_ub, double conf_param=1.96 ) {
            r = Correlation();
            if( m_n > 3 ) {
                double z = atanh(r);
                double sdev = 1.0/sqrt(m_n-3);
                conf_lb = tanh(z-conf_param*sdev);
                conf_ub = tanh(z+conf_param*sdev);
            } else {
                conf_lb = -1.0;
                conf_ub = 1.0;
            }
        }
    private:
        double sum_xy_minus_xbarybar;
        double sum_xy;
        RunningStat x;
        RunningStat y;
        int m_n;
};



class FeatureEnabler {
    public:
        FeatureEnabler( int nfeats_, double conf_param_, double corr_threshold_, bool must_have_same_sign_ ) {
            init( nfeats_, conf_param_, corr_threshold_, must_have_same_sign_ );
        }
        FeatureEnabler( int nfeats_ ) {
            init( nfeats_ );
        }
        FeatureEnabler( std::vector<bool> isFraction_, int minocc_, int minbins_geq_minocc_=3 ) {
            init( isFraction_.size() );
        }
        ~FeatureEnabler() {
        }

        void clear() {
            for( int i = 0; i < nfeats; i++ ) {
                stat_r[i].Clear();
            }
        }
        List update( NumericVector x, int lab );
        std::pair<bool,std::vector<int> > spUpdate( MySpVec x, int lab );
        bool updateCpp( std::vector<double> x, int lab, std::vector<int> &new_feats );

        IntegerVector getEnabled() ;
        unsigned int nfeatures() { return nfeats; };
        void setAdmissionCriterion( double new_z, double new_threshold, bool MUST_HAVE ) {
            conf_param          = new_z;
            corr_threshold      = new_threshold;
            must_have_same_sign = MUST_HAVE;
            fprintf( stderr, "New criterion: corr_threshold:%lf, conf_param:%lf, must_have:%d\n", corr_threshold, conf_param, 1*must_have_same_sign);
        }

    private:
        void init( int nfeats_, double conf_param_=1.96, double corr_threshold_=0.20, bool must_have_same_sign_=true )  {
            nfeats = nfeats_;
            conf_param = conf_param_;
            corr_threshold = corr_threshold_;
            must_have_same_sign = must_have_same_sign_;

            EPSILON = 2e-10;
            counts[0] = counts[1] = 0;

            enabled.resize(nfeats);
            stat_r.resize(nfeats);
            std::fill_n(  enabled.begin(),nfeats,false);
        }
        double EPSILON;

        unsigned int nfeats;
        int counts[2];
        std::vector<RunningCorrelation> stat_r;
        std::vector<bool> enabled;
        double conf_param, corr_threshold;
        bool must_have_same_sign;

};

IntegerVector FeatureEnabler::getEnabled() {
    IntegerVector enabled_;
    for( int i = 0; i < nfeats; i++)
        if(enabled[i])
            enabled_.push_back(i+1);
    return enabled_;
}

List FeatureEnabler::update( NumericVector x, int lab ) {
    bool was_enabled = false;
    IntegerVector new_enabled;

    counts[lab]++;
    for( int i=0; i < nfeats; i++ ) {
        double r = 0.0; // Pearson's correlation coefficient
        double denom = 0.0;
        double conf_lb = 0.0, conf_ub = 0.0;

        // 2. Update correlation (if not enabled)
        if( !enabled[i] ) {
            stat_r[i].Push(x[i],lab*2-1); // converts from {0,1} to {-1,1}
            stat_r[i].CorrelationWithBounds( r, conf_lb, conf_ub, conf_param );
            if( i == 0 ) {
              fprintf( stderr, "+(%.2lf,%d) corr for feature 1: %.2lf in [%.2lf,%.2lf]\n", x[i], lab*2-1, r, conf_lb, conf_ub );
            }
        }
        if( counts[0] + counts[1] < 3 )
            continue;

        if( !enabled[i] ) {
            if( fabs(r)>corr_threshold && (!must_have_same_sign || conf_lb*conf_ub>0) ) {
                enabled[i] = true;
                was_enabled = true;
                new_enabled.push_back(i+1);
                fprintf( stderr, "Enabled feature %d, corr=%lf in [%lf,%lf]\n", i+1, fabs(r), conf_lb, conf_ub);
            }
        }

    }
    return List::create(Named("enabled") = was_enabled, Named("new_enabled") = new_enabled);
}

bool FeatureEnabler::updateCpp( std::vector<double> x, int lab, std::vector<int> &new_enabled ) {
    bool was_enabled = false;

    counts[lab]++;
    for( int i=0; i < nfeats; i++ ) {
        double r = 0.0; // Pearson's correlation coefficient
        double denom = 0.0;
        double conf_lb = 0.0, conf_ub = 0.0;

        // 2. Update correlation (if not enabled)
        if( !enabled[i] ) {
            stat_r[i].Push(x[i],lab*2-1); // converts from {0,1} to {-1,1}
            stat_r[i].CorrelationWithBounds( r, conf_lb, conf_ub, conf_param );
        }
        if( counts[0] + counts[1] < 3 )
            continue;

        if( !enabled[i] ) {
            if( fabs(r)>corr_threshold && (!must_have_same_sign || conf_lb*conf_ub>0) ) {
                enabled[i] = true;
                was_enabled = true;
                new_enabled.push_back(i+1);
            }
        }

    }
    return was_enabled;
}

std::pair<bool,std::vector<int> > FeatureEnabler::spUpdate( MySpVec x, int lab ) {
    bool was_enabled = false;
    std::vector<int> new_enabled;

    counts[lab]++;
    MySpVec::iterator it = x.begin();
    for( int i=0; i < nfeats; i++ ) {
        double r = 0.0; // Pearson's correlation coefficient
        double denom = 0.0;
        double conf_lb = 0.0, conf_ub = 0.0;
        double val = 0.0;

        if( it != x.end() && it->first == i ){
            val = it->second;
            it++;
        }
        // 2. Update correlation (if not enabled)
        if( !enabled[i] ) {
            stat_r[i].Push(val,lab*2-1); // converts from {0,1} to {-1,1}
            stat_r[i].CorrelationWithBounds( r, conf_lb, conf_ub, conf_param );
        }
        if( counts[0] + counts[1] < 3 )
            continue;

        if( !enabled[i] ) {
            if( fabs(r)>corr_threshold && (!must_have_same_sign || conf_lb*conf_ub>0) ) {
                enabled[i] = true;
                was_enabled = true;
                new_enabled.push_back(i+1);
            }
        }

    }
    return std::make_pair(was_enabled, new_enabled);
}




class Graph {
public:
    Graph( bool undirected_ ) : undirected(undirected_), totalE(0), label_max(1),
        target_label(1), recalculate_node_feats(false) { srand48(0); }
    ~Graph() {}
    void addEdge( int u, int v );
    int getN() { return this->out.size(); } // FIXME
    int getE() { return this->totalE; }
    int                 getRandomNode();

    std::vector<int>    getNeighbors( int i ) { std::vector<int> ret(out[i].begin(),out[i].end()); return ret; }
    std::vector<int>    getInNeighbors( int i ) { std::vector<int> ret(in[i].begin(),in[i].end()); return ret; }
    int                 getLabel( int i ) { return label[i]; }
    IntegerVector       getAttributes( int i ) { return wrap(attribs[i]); }

    List                getInfo( int i ) {
        if( share_neighs[i] )
            return List::create(Named("accept")     = (int)accept[i],
                                Named("neighbors")  = wrap(out[i]),
                                Named("label")      = (int)label[i],
                                Named("attributes") = wrap(attribs[i]) );
        else
            return List::create(Named("accept")     = (int)accept[i],
                                Named("label")      = (int)label[i],
                                Named("attributes") = wrap(attribs[i]) );
    }

    //std::vector<double> getAttributes( int i ) { return attribs[i]; }
    std::vector<std::string> getFeatureNames() { return this->feat_names; }
    int nfeatures() { return feat_names.size(); }
    std::vector<bool> isFeatureFraction() { return this->feat_isFraction; }

    //void initFeatures( int ntypes, int target, std::vector<int> att_max );
    void initFeatures( int ntypes, int target, int nattribs );
    void initFeatsBuffer( int rows, int cols) { feats_buffer = MySpMat( rows, cols ); };

    std::pair<std::vector<int>,MySpMat> addNode( int u, std::vector<int> neighs, int label, std::vector<int> attribs );
    NumericMatrix _addNode( int u, IntegerVector neighs, int label, NumericVector attribs );
    SpMat         addNode4( int u, IntegerVector neighs, int label, IntegerVector attribs );
    SpMat         addNode5( int u, int label, IntegerVector attribs ) {
        IntegerVector empty(0);
        return addNode4( u, empty, label, attribs );
    }
    NumericMatrix addNode2( int u, IntegerVector neighs, int label, NumericVector attribs ) {
        return _addNode( u, neighs, label, attribs );
    }
    NumericMatrix addNode3( int u, int label, NumericVector attribs ) {
        IntegerVector empty(0);
        return _addNode( u, empty, label, attribs );
    }

    void readGraph( std::string filename );
    void readCommunities3( std::string filename, int accept_trait, int target_trait,
            double p_accept_one=1.0, double p_accept_zero=0.0, double p_neighs_accept=1.0, double p_neighs_reject=0.0 );
    void readCommunities2( std::string filename, int accept_trait,
            double p_accept_one=1.0, double p_accept_zero=0.0, double p_neighs_accept=1.0, double p_neighs_reject=0.0 ) {
        readCommunities3( filename, accept_trait, accept_trait, p_accept_one, p_accept_zero, p_neighs_accept, p_neighs_reject );
    }
    std::vector<int>getTargetNodes( int nsamples );
    void writeAttributesToFile( std::string filename );

    NumericMatrix computeFeatures( std::vector<int> v ) {
      NumericMatrix feats(nfeatures()+1,v.size());
      std::unordered_set<int> vprime(v.begin(),v.end());

      _computeFeatures( vprime, feats );
      return feats;
    }

    NumericMatrix distanceMatrix( std::vector<int> seed_ids );
    void recalculateNodeFeatures( bool recalculate ) { recalculate_node_feats = recalculate; };

private:
    // TODO: make vertex a class
    std::vector<std::unordered_set<int> > out;
    std::vector<std::unordered_set<int> > in;
    std::vector<char> valid;
    std::vector<char> label;
    std::vector<bool> accept;
    std::vector<bool> share_neighs;
    std::vector<std::vector<int> > attribs; // TODO: make this of type double
    MySpMat feats_buffer;
    bool recalculate_node_feats;

    int label_max, target_label;
    std::vector<std::string> feat_names;
    std::vector<bool> feat_isFraction;
    // TODO: map ids to save space
    //std::map<int,int> old2new;
    //std::map<int,int> new2old;
    int totalE;
    int nattribs;
    int id_offset, max_id;
    bool undirected;
    std::vector<int> target_nodes;

    void _computeFeatures( std::unordered_set<int> v, NumericMatrix &feats );
    void computeFeaturesSparse( std::unordered_set<int> v, SpMat &feats );
};
RCPP_EXPOSED_CLASS(Graph);

std::vector<int> Graph::getTargetNodes( int nsamples ) {
    if( nsamples > target_nodes.size() ) {
        char err_msg[100];
        sprintf( err_msg, "Graph::getTargetNodes() -- nsamples > target_nodes.size() (%d > %lu)\n", nsamples, target_nodes.size()  );
        throw std::runtime_error( err_msg );
    }
    std::vector<int> ret( target_nodes.begin(), target_nodes.end() );
    std::random_shuffle(ret.begin(),ret.end());
    ret.resize(nsamples);

    return ret;
}


int Graph::getRandomNode() {
    int id = (int)(drand48()*out.size());
    while( !valid[id] )
        id = (int)(drand48()*out.size());
    return id;
}

class FileHandler {
    public:
        bool use_gzip;
        FILE *f;

        FILE * open( std::string filename );
        void close();
};

FILE * FileHandler::open( std::string filename ) {
    use_gzip = filename.substr(filename.size()-3,3) == ".gz";
    if( use_gzip ) {
        char st[1000];
        sprintf( st, "gunzip -c %s", filename.c_str() );
        f = popen(st, "r");
    } else {
        f = fopen(filename.c_str(), "r");
    }

    return f;
}

void FileHandler::close( void ) {
    if( use_gzip ) pclose(f);
    else fclose(f);
}

//TODO: modify the code to skip comment lines
void Graph::readGraph( std::string filename ) {
    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t min_id = 1000;
    max_id = -1;
    while(!feof(f)) {
        int32_t i, j;
        if( fscanf(f, "%d %d", &i, &j) ==  2 ) {
            if(i != j) {
                min_id = MIN(min_id,MIN(i,j));
                max_id = MAX(max_id,MAX(i,j));
            }
        }
    }
    fh->close();

    id_offset = min_id-1;
    max_id -= id_offset;
    out.resize(max_id+1);
    in.resize(max_id+1);
    //attribs.resize(max_id+1);
    attribs.resize(max_id+1);
    label.resize(max_id+1);
    valid.resize(max_id+1);
    accept.resize(max_id+1);
    share_neighs.resize(max_id+1);

    std::cout << "id_offset " << id_offset << ", max_id " << max_id << std::endl;

    int32_t count = 0;
    f = fh->open( filename );
    while(!feof(f)) {
        int32_t i, j;
        if( fscanf(f, "%d %d", &i, &j) ==  2 ) {
            if(i != j) {
                i -= id_offset;
                j -= id_offset;
                out[i].insert(j);
                in[j].insert(i);
                totalE++;
                if( undirected ) {
                    out[j].insert(i);
                    in[i].insert(j);
                }
                valid[i] = 1;
                valid[j] = 1;

                if( totalE%100000 == 0 )
                    fprintf( stdout, "Processed %d edges.\n", totalE );
            }
        }
    }
    fh->close();
    delete fh;
}

void Graph::readCommunities3( std::string filename, int accept_trait, int target_trait,
        double p_accept_one, double p_accept_zero, double p_neighs_accept, double p_neighs_reject ) {

    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t community = 0, id, nread;
    char *line;
    size_t max_line_len = 10000;
    line = (char *) malloc(max_line_len*sizeof(char));

    while( (nread = getline( &line, &max_line_len, f)) > -1 ) {
        const char *sep = " \t,";
        char *id_str, *last;

        if( line[0] == '#' ) // skip headers
            continue;

        for( id_str = strtok_r(line, sep, &last);
             id_str;
             id_str = strtok_r(NULL, sep, &last) ) {
            id = atoi(id_str);
            if(community == accept_trait) {
                accept[id-id_offset] = true; // att corresponding to label is not included, on purpose
            }
            if( community == target_trait ) {
                label[id-id_offset] = 1;
                target_nodes.push_back(id-id_offset);
            }
            attribs[id-id_offset].push_back(community);
        }
        community++;
    }
    free(line);

    // if probabilistic, flip coins to determine acceptance
    if( p_accept_one < 1 || p_accept_zero > 0 ) {
        double prob[] = { p_accept_zero, p_accept_one };
        for( int i = 0; i < label.size(); i++ )
            if( valid[i] ) {
                accept[i] = ( drand48() <= prob[accept[i]] );
                if( accept_trait == target_trait ) { // objective function is max accepts, so accept <==> label
                    label[i] = 1*accept[i];
                }
            }
    }

    // if probabilistic, flip coins to determine whether node shares neighbors
    if( p_neighs_accept < 1 || p_neighs_reject < 1 ) {
        double prob[] = { p_neighs_reject, p_neighs_accept };
        for( int i = 0; i < label.size(); i++ )
            if( valid[i] )
                share_neighs[i] = ( drand48() < prob[accept[i]] );
    } else {
        for( int i = 0; i < label.size(); i++ )
            if( valid[i] )
                share_neighs[i] = true;
    }

    //fprintf( stdout, "Largest community: %d (%d members)\n", largest_community, max_k );

    //while(!feof(f)) {
    //    fprintf( stdout, "Community %d:", community );
    //    while( fscanf( f, "%d[^\n]", &id ) == 1 ) {
    //        fprintf( stdout, " %d", id);
    //        attribs[id-id_offset][community] = 1;

    //        if(community == target)
    //            label[id-id_offset]++;
    //    }
    //    fprintf( stdout, "\n");
    //    community++;
    //}
    fh->close();
    delete fh;

}

void Graph::writeAttributesToFile( std::string filename ) {
    std::ofstream outfile;
    outfile.open(filename );
    for( int i = 0; i < out.size(); i++ ) {
        outfile << attribs[i][0];
        for( int j = 1; j < nattribs; j++ ) {
            outfile << "\t" << attribs[i][j];
        }
        if( i < out.size()-1 )
            outfile << "\n";
    }
    outfile.close();
}

std::pair<std::vector<int>,MySpMat> Graph::addNode( int u, std::vector<int> neighs, int label, std::vector<int> attribs) {
    // 1. get max and resize vectors
    int max = u;
    for( int i = 0; i < neighs.size(); i++ )
        max = MAX( max, neighs[i] );
    int cur_max = this->out.size();
    if( cur_max < (max+1) ) {
        //int new_max = MAX(cur_max*2,max+1);
        int new_max = max+1;
        this->out.resize(new_max);
        this->in.resize(new_max);
        this->label.resize(new_max);
        this->attribs.resize(new_max);
        this->valid.resize(new_max);
    }

    valid[u] = 1; // node is recruited
    
    // 2. add edges and mark border nodes to have its features computed
    std::unordered_set<int> toBeComputed, toBeUpdated;
    for( int i = 0; i < neighs.size(); i++ ) {
        this->out[u].insert(neighs[i]);
        this->in[neighs[i]].insert(u);

    }
    // mark border nodes to have its features computed
    for( std::unordered_set<int>::iterator i=out[u].begin(); i != out[u].end(); i++ ) {
        int v = *i;
        // border node
        if( !valid[v] )
            toBeComputed.insert(v);
        //else {
        //  for( std::unordered_set<int>::iterator j=out[v].begin(); j != out[v].end(); j++ )
        //    if( !valid[*j] )
        //      toBeUpdated.insert(*j);
        //}
    }


    // 3. set label and attribs
    this->label[u] = label;
    this->attribs[u] = attribs; //copy

    int nrows = toBeComputed.size()+toBeUpdated.size();
    int ncols = feat_names.size()+1;
    std::vector<int> indices(nrows);
    MySpMat M(nrows,ncols);
    double eps = 1e-10, epsilon;
    double intercept = 1.0;

    for(std::unordered_set<int>::iterator id_it = out[u].begin(); id_it != out[u].end(); ++id_it ) {
        std::vector<double> full = feats_buffer[*id_it];
        full[label] += 1.0;
        feats_buffer.insert(full,*id_it); // write to buffer
    }

    // 4. change values in feats_buffer for all features, except for "yi,most,1", "yi,most-fraction,1", "y,rw,2,5"
    // the calculation of these features require other features to be up-to-date, thus, is performed in step (5)
    int v_ind = 0;
    for(std::unordered_set<int>::iterator id_it = toBeComputed.begin(); id_it != toBeComputed.end(); ++id_it ) {

        int nobserved = in[*id_it].size();
        std::vector<double> full = feats_buffer[*id_it];
        int feat_ind = 0;

        for( std::vector<std::string>::iterator it = feat_names.begin();
                it != feat_names.end(); it++ ) {
            /* process feat name */
            std::istringstream f(*it);
            std::string x1, x2, x3, x4;
            std::getline( f, x1, ',' );
            std::getline( f, x2, ',' );
            if( x2.compare(0,3,"att") != 0 ) // not for attributes
                std::getline( f, x3, ',' );
            if( x2.compare(0,3,"tri") == 0 || x2.compare("rw") == 0 ) // only for triangles, tri-fraction and rw
                std::getline( f, x4, ',' );

            if( x2.compare("counts") == 0 && x3.compare("0") == 0 && x1.compare("y") == 0 ) {
                // counts-fraction
                epsilon = nobserved > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ ) {
                  full[feat_ind+(label_max+1)+val] = (full[feat_ind+val]+epsilon)/(nobserved+(label_max+1)*epsilon);
                }
            } else if( x2.compare("triangles") == 0 && x3[0] == '~' && x4[0] == '~' && x1.compare("y") == 0 ) {
                //for( int val = 0; val <= 2; val++ )
                //    full[feat_ind+val] = feats_buffer.get(*id_it,feat_ind+val);
                if( nobserved > 1 )
                    for( std::unordered_set<int>::iterator a = in[*id_it].begin(); a != in[*id_it].end(); a++ ) {
                        if( u != *a && out[*a].find(u) != out[*a].end() ) { // if new node makes triangle with already-recruited neighbor
                            int val = (this->label[*a] == target_label ) + (label == target_label );
                            full[feat_ind+val] += 1.0;
                        }
                    }

                // tri-fraction
                double ntriangles = 0;
                for( int val = 0; val <= 2; val++ )
                  ntriangles += full[feat_ind+val];
                epsilon = ntriangles > 0 ? 0.0 : eps;
                for( int val = 0; val < 2; val++ ) {
                  full[feat_ind+3+val] = (full[feat_ind+val]+epsilon)/(ntriangles+3*epsilon);
                }
            } else if( x1.compare("yi") == 0 && x2.compare("most") == 0 && x3.compare("0") == 0 ) {
                int counts = 0;
                for( std::unordered_set<int>::iterator a = in[*id_it].begin(); a != in[*id_it].end(); a++ ) {
                  if( feats_buffer.get(*a,0) > feats_buffer.get(*a,1) ) {
                    counts++;
                  }
                }
                full[feat_ind] = counts;

                // most-fraction
                epsilon = nobserved > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ ) {
                  full[feat_ind+1+val] = (counts+epsilon)/(nobserved+(label_max+1)*epsilon);
                }
            } else if( x1.compare(0,4,"att0") == 0 && x2.compare("average") == 0 ) {
                if( nobserved > 1 ) { // renormalize by new nobserved
                  //printf( "Renormalizing, nobserved = %d\n", nobserved );
                  MySpVec feats = feats_buffer.get(*id_it);
                  for( MySpVec::iterator it = feats.begin(); it != feats.end(); ++it )
                      if( it->first >= feat_ind && it->first <= feat_ind+nattribs)
                        full[it->first] = it->second*(1.0-1.0/nobserved);
                }

                if( nobserved > 0 ) {
                  //printf( "Add %lf to features:", 1.0/nobserved );
                  for( std::vector<int>::iterator it = attribs.begin(); it != attribs.end(); ++it ) {
                    //printf( " %d", feat_ind+*it );
                    full[feat_ind+*it] += 1.0/nobserved;
                    //full[feat_ind+*it] = (full[feat_ind+*it]*(nobserved-1) + 1)/nobserved;
                  }
                  //printf( "\n" );
                }

            } else if( x2.compare("rw") == 0 && x1.compare("y") == 0 ) {
                /* Assuming 2 steps */
                double sum = 0.0;
                int denom = nobserved;
                for( std::unordered_set<int>::iterator a = in[*id_it].begin(); a != in[*id_it].end(); a++ ) {         // recruited neighs
                  double neighs0 = feats_buffer.get(*a,0);
                  double neighs1 = feats_buffer.get(*a,1);
                  if( neighs0+neighs1 > 0 )
                    sum += neighs1/(neighs0+neighs1);
                  else
                    denom--;
                }
                if( denom > 0 ) {
                  full[feat_ind] = sum/denom;
                }
            }
            feat_ind++;
        }
        feats_buffer.insert(full,*id_it); // write to buffer
        indices[v_ind]= *id_it;      // save vertex id
        M.insert(full,v_ind++,intercept);      // save vertex feats
    }

    for(std::unordered_set<int>::iterator id_it = toBeUpdated.begin(); id_it != toBeUpdated.end(); ++id_it ) {
        int nobserved = in[*id_it].size();
        std::vector<double> full = feats_buffer[*id_it];

        int feat_ind = 0;
        for( std::vector<std::string>::iterator it = feat_names.begin();
                it != feat_names.end(); it++ ) {
            /* process feat name */
            std::istringstream f(*it);
            std::string x1, x2, x3, x4;
            std::getline( f, x1, ',' );
            std::getline( f, x2, ',' );
            if( x2.compare(0,3,"att") != 0 ) // not for attributes
                std::getline( f, x3, ',' );
            if( x2.compare(0,3,"tri") == 0 || x2.compare("rw") == 0 ) // only for triangles, tri-fraction and rw
                std::getline( f, x4, ',' );

            if( x1.compare("yi") == 0 && x2.compare("most") == 0 && x3.compare("0") == 0 ) {
                int counts = 0;
                for( std::unordered_set<int>::iterator a = in[*id_it].begin(); a != in[*id_it].end(); a++ ) {         // recruited neighs
                  if( feats_buffer.get(*a,0) > feats_buffer.get(*a,1) ) // TODO: instead of 0, 1 use attribute names
                    counts++;
                }
                full[feat_ind] = counts;

                // most-fraction
                epsilon = nobserved > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ ) {
                  full[feat_ind+1+val] = (counts+epsilon)/(nobserved+(label_max+1)*epsilon);
                }
            } else if( x2.compare("rw") == 0 && x1.compare("y") == 0 ) {
                /* Assuming 2 steps */
                double sum = 0.0;
                int denom = nobserved;
                for( std::unordered_set<int>::iterator a = in[*id_it].begin(); a != in[*id_it].end(); a++ ) {         // recruited neighs
                  double neighs0 = feats_buffer.get(*a,0);
                  double neighs1 = feats_buffer.get(*a,1);
                  if( neighs0+neighs1 > 0 )
                    sum += neighs1/(neighs0+neighs1);
                  else
                    denom--;
                }
                if( denom > 0 )
                  full[feat_ind] = sum/denom;
            }
            feat_ind++;
        }

        feats_buffer.insert(full,*id_it); // write to buffer
        indices[v_ind]= *id_it;      // save vertex id
        M.insert(full,v_ind++,intercept);      // save vertex feats
    }


    return std::make_pair( indices, M );
}

SpMat      Graph::addNode4( int u, IntegerVector neighs, int label, IntegerVector attribs) {
    // get max and resize vectors
    int max = u;
    for( int i = 0; i < neighs.size(); i++ )
        max = MAX( max, neighs[i] );
    int cur_max = this->out.size();
    if( cur_max < (max+1) ) {
        //int new_max = MAX(cur_max*2,max+1);
        int new_max = max+1;
        this->out.resize(new_max);
        this->in.resize(new_max);
        this->label.resize(new_max);
        this->attribs.resize(new_max);
        this->valid.resize(new_max);
    }

    valid[u] = 1; // node is recruited
    
    std::unordered_set<int> toBeComputed;
    //add edges and mark border nodes to have its features computed
    for( int i = 0; i < neighs.size(); i++ ) {
        this->out[u].insert(neighs[i]);
        this->in[neighs[i]].insert(u);

    }

    // mark border nodes to have its features computed
    for( std::unordered_set<int>::iterator i=out[u].begin(); i != out[u].end(); i++ ) {
        int v = *i;
        // border node
        if( !valid[v] || recalculate_node_feats )
            toBeComputed.insert(v);
        //// border neighbor of recruited neighbor
        //else { //TODO: uncomment this
        //    for( std::unordered_set<int>::iterator j=out[v].begin(); j != out[v].end(); j++ )
        //        if( !valid[*j] )
        //            toBeComputed.insert(*j);
        //}
    }


    //set label and attribs
    this->label[u] = label;
    this->attribs[u] = as<std::vector<int> >(attribs); //copy

    int i = 0;
    int nrows, ncols;
    nrows = feat_names.size()+1;
    ncols = toBeComputed.size();

    SpMat M(nrows,ncols);

    computeFeaturesSparse(toBeComputed,M);
    return M;

}

//TODO: if id's are sequential, drop parameter and use push_back instead of resize
NumericMatrix      Graph::_addNode( int u, IntegerVector neighs, int label, NumericVector attribs ) {
    // get max and resize vectors
    int max = u;
    for( int i = 0; i < neighs.size(); i++ )
        max = MAX( max, neighs[i] );
    int cur_max = this->out.size();
    if( cur_max < (max+1) ) {
        int new_max = MAX(cur_max*2,max+1);
        this->out.resize(new_max);
        this->in.resize(new_max);
        this->label.resize(new_max);
        this->attribs.resize(new_max);
        this->valid.resize(new_max);
    }

    //fprintf(stderr, "B1\n" );
    valid[u] = 1; // node is recruited
    
    std::unordered_set<int> toBeComputed;
    //add edges and mark border nodes to have its features computed
    for( int i = 0; i < neighs.size(); i++ ) {
        this->out[u].insert(neighs[i]);
        this->in[neighs[i]].insert(u);

    }

    //fprintf(stderr, "B2\n" );
    // mark border nodes to have its features computed
    for( std::unordered_set<int>::iterator i=out[u].begin(); i != out[u].end(); i++ ) {
        int v = *i;
        // border node
        if( !valid[v] )
            toBeComputed.insert(v);
        // border neighbor of recruited neighbor
        else {
            for( std::unordered_set<int>::iterator j=out[v].begin(); j != out[v].end(); j++ )
                if( !valid[*j] )
                    toBeComputed.insert(*j);
        }
    }


    //fprintf(stderr, "B3\n" );
    //set label and attribs
    this->label[u] = label;
    this->attribs[u] = as<std::vector<int> >(attribs); //copy

    int i = 0;
    int nrows, ncols; //colMajor
    nrows = feat_names.size()+1;
    ncols = toBeComputed.size();
    NumericMatrix M(nrows,ncols);

    //if( colMajor)
        _computeFeatures(toBeComputed, M);
    //else
    //    computeFeatures2(toBeComputed, M);
    return M;

}

void Graph::computeFeaturesSparse( std::unordered_set<int> toBeComputed, SpMat &M ) { //colMajor version
    std::vector<int> entries;
    for( std::unordered_set<int>::iterator v=toBeComputed.begin(); v != toBeComputed.end(); v++ ) {
            int count = 12;
            for( std::unordered_set<int>::iterator u = in[*v].begin(); u != in[*v].end(); u++) {
                count += attribs[*u].size();
            }
            entries.push_back(count);
    }
    if(entries.empty()) return;
    else M.reserve(entries);

    int v_ind=0;
    for( std::unordered_set<int>::iterator it=toBeComputed.begin();
            it != toBeComputed.end(); it++ ) {
        int v = *it;
        //double *x = new double[feat_names.size()]();
        double eps = 1e-10, epsilon;
        std::unordered_set<int> observed_neighs = in[v]; //construct set
        for( std::unordered_set<int>::iterator u = out[v].begin(); u != out[v].end(); u++)
            if( valid[*u] )
                observed_neighs.insert(*u);


        int feat_ind = 0;
        //printf("M[%d,%d] = %d\n", feat_ind,v_ind,v);
        M.insert(feat_ind++,v_ind) = v;
        for( std::vector<std::string>::iterator it = feat_names.begin();
                it != feat_names.end(); it++ ) {

            // read and parse feat_name
            std::istringstream f(*it);
            std::string x1, x2, x3, x4;
            std::getline( f, x1, ',' );
            std::getline( f, x2, ',' );
            if( x2.compare(0,3,"att") != 0 ) // not for attributes
                std::getline( f, x3, ',' );
            if( x2.compare(0,3,"tri") == 0 || x2.compare("rw") == 0 ) // only for triangles, tri-fraction and rw
                std::getline( f, x4, ',' );
                
            if( x2.compare("counts") == 0 && x3.compare("0") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "counts\n");
                // counts
                int counts[] = {0,0};
                for( std::unordered_set<int>::iterator u = observed_neighs.begin(); u != observed_neighs.end(); u++ ) {
                    counts[label[*u]]++;
                    //fprintf( stderr, "feat_ind %d label %d\n", feat_ind, label[*u]);
                }
                for( int val = 0; val <= label_max; val++ )
                    M.insert(feat_ind+val,v_ind) = counts[val];
                
                // counts-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ ) {
                    //fprintf( stderr, "feat_ind %d val %d\n", feat_ind, val);
                    M.insert(feat_ind+(label_max+1)+val,v_ind ) = (counts[val]+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
                }
            } else if( x2.compare("triangles") == 0 && x3[0] == '~' && x4[0] == '~' && x1.compare("y") == 0 ) {
                //fprintf( stderr, "triangles\n");
                // triangles
                int tmp = 0;
                int counts[] = {0,0,0};
                if( observed_neighs.size() > 0 )
                    for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                        for( std::unordered_set<int>::iterator b = a; b != observed_neighs.end(); b++ ) {
                            if( out[*a].find(*b) != out[*a].end() ||
                                out[*b].find(*a) != out[*b].end() ) {
                                int pos = (label[*a] == target_label ) + (label[*b] == target_label );
                                counts[pos]++;
                                tmp++;
                            }
                        }
                    }
                for( int val = 0; val <= 2; val++ )
                    M.insert(feat_ind+val,v_ind) = counts[val];
                // tri-fraction
                epsilon = tmp > 0 ? 0.0 : eps;
                for( int val = 0; val < 2; val++ ) {
                    //fprintf( stderr, "Setting pos %d to %lf/%lf = %lf\n", feat_ind+3+val,
                    //        (M(v_ind,feat_ind+val)+epsilon),(tmp+3*epsilon),
                    //        (M(v_ind,feat_ind+val)+epsilon)/(tmp+3*epsilon) );
                    M.insert(feat_ind+3+val,v_ind) = (counts[val]+epsilon)/(tmp+3*epsilon);
                }
            } else if( x1.compare("yi") == 0 && x2.compare("most") == 0 && x3.compare("0") == 0 ) {
                int counts = 0;
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    int tmp[] = {0,0};
                    for( std::unordered_set<int>::iterator b = in[*a].begin(); b != in[*a].end(); b++ )
                        tmp[label[*b]]++;

                    if (tmp[0] > tmp[1])
                        counts++;
                }
                M.insert(feat_ind,v_ind) = counts;
                // most-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ )
                    M.insert(feat_ind+1+val,v_ind) = (counts+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);

            } else if( x1.compare(0,4,"att0") == 0 && x2.compare("average") == 0 ) {
                double inc = 1.0/MAX(1,observed_neighs.size());
                int *counts = new int [nattribs]();
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    for( int j = 0; j < attribs[*a].size(); j++ )
                        if( attribs[*a][j] < nattribs )
                            counts[attribs[*a][j]]++;
                }
                for( int j = 0; j < nattribs; j++ )
                    if( counts[j]>0 )
                        M.insert(feat_ind+j,v_ind) = counts[j]*1.0*inc;
                delete counts;

            } else if( x2.compare("rw") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "rw\n");
                int steps = atoi(x3.c_str());
                int times = atoi(x4.c_str());

                double sum = 0.0, denom = 0.0;
                for( int t = 0; t < times; t++ ) {
                    int rw_pos = v;
                    int s;
                    for( s = 0; s < steps; s++ ) {
                        if( in[rw_pos].size() == 0 )
                            break;
                        double p_in;
                        p_in = in[rw_pos].size()/(in[rw_pos].size()+out[rw_pos].size());
                        p_in = 1.0;
                        if( drand48() <= p_in ) {
                            std::unordered_set<int>::iterator it = in[rw_pos].begin();
                            std::advance(it,(int)(drand48()*in[rw_pos].size()));
                            rw_pos = *it;
                        } else {
                            std::unordered_set<int>::iterator it = out[rw_pos].begin();
                            std::advance(it,(int)(drand48()*out[rw_pos].size()));
                            rw_pos = *it;
                        }
                    }
                    if( s == steps ) {
                        sum += label[rw_pos];
                        denom += 1.0;
                    }
                }
                if( denom > 0 )
                    M.insert(feat_ind,v_ind) = sum/denom;
            }
            feat_ind++;
        }
        //for( int i = 0; i < feat_names.size(); i++ )
        //    feats[i] = x[i];
        //delete x;
        v_ind++;
    }
    M.makeCompressed();

    //IntegerVector coef(wrap(Eigen::MatrixXd(M.block(0,0,1,11))));
}

NumericMatrix Graph::distanceMatrix( std::vector<int> seed_ids ) {

  NumericMatrix M(seed_ids.size(),seed_ids.size());
  std::map<int,int> v2ind;
  for( int i=0; i < seed_ids.size(); i++ )
    v2ind[seed_ids[i]] = i;

  for( std::vector<int>::iterator it = seed_ids.begin(); it != seed_ids.end(); ++it ) {
    std::set<int> targets(seed_ids.begin(),seed_ids.end());
    std::deque<int> q;
    std::map<int,int> marked;

    q.push_back(*it);
    marked[*it] = 1;
    targets.erase(*it);
    M(v2ind[*it],v2ind[*it]) = marked[*it]-1;

    while( !targets.empty() && !q.empty() ) {
      int v = q.front(); q.pop_front();
      for( std::unordered_set<int>::iterator it2 = out[v].begin(); it2 != out[v].end(); it2++) {
        int u = *it2;
        if( marked[u] == 0 ) {
          //std::cerr << "Wave " << marked[v] << ", node " << u << std::endl; 
          marked[u] = marked[v]+1;
          q.push_back(u);

          std::set<int>::iterator elem = targets.find(u);
          if( elem != targets.end() ) {
            targets.erase(elem);
            M(v2ind[*it],v2ind[u]) = marked[u]-1;
          }
        }
      }
    }
  }

  return M;
}


void Graph::_computeFeatures( std::unordered_set<int> toBeComputed, NumericMatrix &M ) { //colMajor version

    int v_ind=0;
    for( std::unordered_set<int>::iterator it=toBeComputed.begin();
            it != toBeComputed.end(); it++ ) {
        int v = *it;
        //double *x = new double[feat_names.size()]();
        double eps = 1e-10, epsilon;
        std::unordered_set<int> observed_neighs = in[v]; //construct set
        for( std::unordered_set<int>::iterator u = out[v].begin(); u != out[v].end(); u++)
            if( valid[*u] )
                observed_neighs.insert(*u);

        int feat_ind = 0;
        M(feat_ind++,v_ind) = v;
        for( std::vector<std::string>::iterator it = feat_names.begin();
                it != feat_names.end(); it++ ) {

            // read and parse feat_name
            std::istringstream f(*it);
            std::string x1, x2, x3, x4;
            std::getline( f, x1, ',' );
            std::getline( f, x2, ',' );
            if( x2.compare(0,3,"att") != 0 ) // not for attributes
                std::getline( f, x3, ',' );
            if( x2.compare(0,3,"tri") == 0 || x2.compare("rw") == 0 ) // only for triangles, tri-fraction and rw
                std::getline( f, x4, ',' );
                
            if( x2.compare("counts") == 0 && x3.compare("0") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "counts\n");
                // counts
                for( std::unordered_set<int>::iterator u = observed_neighs.begin(); u != observed_neighs.end(); u++ ) {
                    //fprintf( stderr, "feat_ind %d label %d\n", feat_ind, label[*u]);
                    M( feat_ind+label[*u] ,v_ind) += 1.0;

                }
                // counts-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ ) {
                    //fprintf( stderr, "feat_ind %d val %d\n", feat_ind, val);
                    M(feat_ind+(label_max+1)+val,v_ind ) = (M(feat_ind+val,v_ind  )+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
                }
            } else if( x2.compare("triangles") == 0 && x3[0] == '~' && x4[0] == '~' && x1.compare("y") == 0 ) {
                //fprintf( stderr, "triangles\n");
                // triangles
                int tmp = 0;
                if( observed_neighs.size() > 0 )
                    for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                        for( std::unordered_set<int>::iterator b = a; b != observed_neighs.end(); b++ ) {
                            if( out[*a].find(*b) != out[*a].end() ||
                                out[*b].find(*a) != out[*b].end() ) {
                                int pos = (label[*a] == target_label ) + (label[*b] == target_label );
                                M(feat_ind+pos,v_ind ) += 1.0;
                                tmp++;
                            }
                        }
                    }
                // tri-fraction
                epsilon = tmp > 0 ? 0.0 : eps;
                for( int val = 0; val < 2; val++ ) {
                    //fprintf( stderr, "Setting pos %d to %lf/%lf = %lf\n", feat_ind+3+val,
                    //        (M(v_ind,feat_ind+val)+epsilon),(tmp+3*epsilon),
                    //        (M(v_ind,feat_ind+val)+epsilon)/(tmp+3*epsilon) );
                    M(feat_ind+3+val,v_ind) = (M(feat_ind+val,v_ind)+epsilon)/(tmp+3*epsilon);
                }
            } else if( x1.compare("yi") == 0 && x2.compare("most") == 0 && x3.compare("0") == 0 ) {
                //fprintf( stderr, "most\n");
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    int count[label_max+1];

                    std::unordered_set<int> observed_neighs_a = in[*a]; //construct set
                    for( std::unordered_set<int>::iterator it = out[*a].begin(); it != out[*a].end(); it++)
                        if( valid[*it] )
                            observed_neighs_a.insert(*it);

                    for( std::unordered_set<int>::iterator b = observed_neighs_a.begin(); b != observed_neighs_a.end(); b++ ) {
                        count[label[*b]]++;
                    }
                    int max_ind = -1, max_val = -1;
                    for( int j = 0; j <= label_max; j++ )
                        if( count[j] > max_val ) {
                            max_ind = j;
                            max_val = count[j];
                        }
                    M(feat_ind+max_ind,v_ind )++;
                }
                // most-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ )
                    M(feat_ind+2+val,v_ind) = (M(feat_ind+val,v_ind)+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
            } else if( x1.compare(0,4,"att0") == 0 && x2.compare("average") == 0 ) {
                double inc = 1.0/MAX(1,observed_neighs.size());
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    for( int j = 0; j < attribs[*a].size(); j++ )
                        if( attribs[*a][j] < nattribs )
                            M(feat_ind+attribs[*a][j],v_ind) += inc;
                }
            } else if( x2.compare("rw") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "rw\n");
                int steps = atoi(x3.c_str());
                int times = atoi(x4.c_str());

                double sum = 0.0;
                for( int t = 0; t < times; t++ ) {
                    int rw_pos = v;
                    for( int s = 0; s < steps; s++ ) {
                        if( in[rw_pos].size() == 0 )
                            break;
                        double p_in;
                        p_in = in[rw_pos].size()/(in[rw_pos].size()+out[rw_pos].size());
                        p_in = 1.0;
                        if( drand48() <= p_in ) {
                            std::unordered_set<int>::iterator it = in[rw_pos].begin();
                            std::advance(it,(int)(drand48()*in[rw_pos].size()));
                            rw_pos = *it;
                        } else {
                            std::unordered_set<int>::iterator it = out[rw_pos].begin();
                            std::advance(it,(int)(drand48()*out[rw_pos].size()));
                            rw_pos = *it;
                        }
                    }
                    sum += label[rw_pos];
                }
                M(feat_ind,v_ind) = sum/times;
            }
            feat_ind++;
        }
        //for( int i = 0; i < feat_names.size(); i++ )
        //    feats[i] = x[i];
        //delete x;
        v_ind++;
    }
}

void Graph::addEdge( int u, int v ) {
    int max = (u>v) ? u : v;
    // make resizing more efficient
    if( this->out.size() < (max+1) ) {
        this->out.resize(max+1);
        this->in.resize(max+1);
        this->label.resize(max+1);
    }
    this->out[u].insert(v);
    this->in[v].insert(u);
    totalE++;
}


//void Graph::initFeatures( int ntypes, int target, std::vector<int> att_max ) {
void Graph::initFeatures( int ntypes, int target, int nattribs ) {
    long long int target2 = target;
    this->nattribs = nattribs;

    //2) counts of neighbors
    for( long long int i=0; i < ntypes; i++ ) {
        feat_names.push_back( std::string("y,counts,") + std::to_string(i));
        feat_isFraction.push_back( false );
    }

    //1) fraction of neighbors
    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("y,counts-fraction,") + std::to_string(i));
        feat_isFraction.push_back( true );
    }

    if( nattribs < 0 ) return;

    feat_names.push_back( std::string("y,triangles") + ",~"+ std::to_string(target2)+",~"+std::to_string(target2));
    feat_names.push_back( std::string("y,triangles") + ",~"+ std::to_string(target2)+","+std::to_string(target2));
    //feat_names.push_back( std::string("y,triangles") + ","+ std::to_string(target2)+",~"+std::to_string(target2));
    feat_names.push_back( std::string("y,triangles") + ","+ std::to_string(target2)+","+std::to_string(target2));
    for( long long int i=0; i < 3; i++ )
        feat_isFraction.push_back( false );

    feat_names.push_back( std::string("y,tri-fraction") + ",~"+ std::to_string(target2)+",~"+std::to_string(target2));
    feat_names.push_back( std::string("y,tri-fraction") + ",~"+ std::to_string(target2)+","+std::to_string(target2));
    for( long long int i=0; i < 2; i++ )
        feat_isFraction.push_back( true );
    //feat_names.push_back( std::string("y,tri-fraction") + ","+ std::to_string(target2)+",~"+std::to_string(target2));

    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("yi,most,") + std::to_string(i));
        feat_isFraction.push_back( false );
    }

    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("yi,most-fraction,") + std::to_string(i));
        feat_isFraction.push_back( true );
    }

    if( INCLUDE_RW ) {
        feat_names.push_back( std::string("y,rw,2,5"));
        feat_isFraction.push_back( true );
    }

    for( long long int att_id=0; att_id < nattribs; att_id++ ) {
        feat_names.push_back( std::string("att")+std::to_string(att_id)+",average");
        feat_isFraction.push_back( true );
    }
}

class ModelTracker {
    public:
        ModelTracker( int max_models_, int nfeatures_, bool sparse_ ) :
            max_models(max_models_), nfeatures(nfeatures_), sparse(sparse_) {
            valid = new bool[max_models]();
            perfectly_separable = new bool[max_models]();
            timestamp = new int[max_models]();
            successes = new int[max_models]();
            uses = new int[max_models]();
            mmat = new IntegerMatrix(nfeatures,max_models);
            models = new List(max_models);
            nvalid = 0;
        }
        ~ModelTracker() {
            delete valid;
            delete perfectly_separable;
            delete timestamp;
            delete successes;
            delete uses;
            delete mmat;
            delete models;
        }
        bool anyTrained() { return nvalid>0; }
        int  nmodels() { return nvalid; }
        bool *isPerfectlySeparable() { return perfectly_separable; }
        bool isPerfectlySeparable( int ind ) { return perfectly_separable[ind]; }
        bool *isModelValid() { return valid; }
        bool isModelValid( int ind ) { return valid[ind]; }
        void resetStats( int ind, int t ) {
          if( !valid[ind] ) nvalid++;
          valid[ind] = true;
          timestamp[ind] = t;
          successes[ind] = 0;
          uses[ind] = 0;
        }
        void setPerfectlySeparable( int ind, bool value ) { perfectly_separable[ind] = value; };

        //void enableFeature( int model_ind, int feat_ind) { (*mmat)(feat_ind-1,model_ind) = 1; };
        void enableFeature( int model_ind, IntegerVector feat_inds) {
            for( int i=0; i < feat_inds.size(); i++ )
                (*mmat)(feat_inds[i]-1,model_ind) = 1;
        };
        void enableFeature( int model_ind, std::vector<int> feat_inds) {
            for( int i=0; i < feat_inds.size(); i++ )
                (*mmat)(feat_inds[i]-1,model_ind) = 1;
        };
        IntegerMatrix *getModelFeats(){ return mmat; }
        IntegerVector getModelFeats( int ind ){ return (*mmat)(_,ind); }
        void duplicate( int orig, int dest ) {
            (*mmat)(_,dest) = (*mmat)(_,orig);
        }

        List getModels(){ return *models; };
        void setModels( List model ){ *models = model; };
        void update( std::vector<int> model_inds, int label ) {
            for( std::vector<int>::iterator it = model_inds.begin(); it != model_inds.end(); ++it ) {
                successes[*it] += label;
                uses[*it]++;
            }
        }
        void update( int model_ind, int label ) {
            successes[model_ind] += label;
            uses[model_ind]++;
          printf("mtracker::update model_ind = %d, label=%d, successes = %d, uses=%d\n", model_ind, label, successes[model_ind],uses[model_ind]);
        }
        int getBestModel( int t ) {
            int best = 0;
            double max_score = -std::numeric_limits<double>::max();
            printf( "Performances:");
            for( int i = 0; i < max_models; i++ ) {
                double score = scoreModel(i,t);
                printf( "\t%e", score);
                if( score > max_score ) {
                    max_score = score;
                    best = i;
                }
            }
            printf( "\n\n");
            return best;
        }

        std::vector<std::pair<int,int> > pairBestWorst(int t) {
            std::vector<std::pair<int,int> > best_worst;
            const double max_score = -std::numeric_limits<double>::max();

            /* if nvalid = 0, duplicate 0 to 0
             * if nvalid < max_models, simply take valid models and duplicate to next available position
             * if nvalid = max_models, score models and make pairs */
            if( nvalid == max_models ) {
                std::vector<ScoreModelPair> scores;
                printf("Successes/Uses:");
                for( int i = 0; i < nvalid; i++ ) {
                  double score = uses[i]>0 ? -(1.0*successes[i]/uses[i]) : -1.0*max_score;
                  scores.push_back(std::make_pair( score, i ) );
                  printf( "\t%e", score);
                }
              printf( "\n\n");
                std::sort( scores.begin(), scores.end(), compareModels );  // sort by score

                for( int j = 0; j < MAX(nvalid/2,1); j++ ) {                         // create pairs
                    best_worst.push_back(std::make_pair( scores[j].second, scores[nvalid/2+j].second ) );
                }
            } else if( nvalid > 0 ) {
                std::vector<ScoreModelPair> scores;
                printf("Successes/Uses:");
                for( int i = 0; i < nvalid; i++ ) {
                double score = uses[i]>0 ? (1.0*successes[i]/uses[i]): max_score;
                  printf( "\t%e", score);
                  scores.push_back(std::make_pair( score, i ) );
                }
              printf( "\n\n");
                std::sort( scores.begin(), scores.end(), compareModels );  // sort by score

                for( int i = 0; i < nvalid; i++ )
                    best_worst.push_back(std::make_pair(scores[i].second,nvalid+i));
            } else {
                best_worst.push_back(std::make_pair(0,0));
            }
            return best_worst;
        }

        void flagModelFeats( int model_ind, std::vector<int> flaggedfeats ) {
            flaggedFeatures.insert(flaggedfeats.begin(),flaggedfeats.end());
            for( int model_ind = 0; model_ind < max_models; model_ind++ ) // FIXME: design a more clever mechanism
                for( std::vector<int>::iterator it = flaggedfeats.begin(); it != flaggedfeats.end(); ++it ) {
                    (*mmat)(*it-1,model_ind) = 0;
                }
        }
    private:
        int max_models;
        int nfeatures;
        bool sparse;
        bool * valid;
        bool * perfectly_separable;
        int  * successes;
        int  * uses;
        int  * timestamp;
        int nvalid;
        IntegerMatrix * mmat;
        List *models;
        double scoreModel( int i, int t, double eps=0.0 ) {
            if( !valid[i] )
                return -std::numeric_limits<double>::max();
            else if( uses[i] > 0 )
                return 1.0*successes[i]/uses[i] + sqrt(2*log(t-timestamp[i])/uses[i]) + eps*uses[i];
            else
                return std::numeric_limits<double>::max();
        }
        std::set<int> flaggedFeatures;

};

class TrainSet {
    public:
        TrainSet( int nfeatures, double var_ ) : noise_var(var_) {
            x = new NumericMatrix( 1000, nfeatures ); // initial size
            y = new std::vector<int>;
            nobs = 0;
        }
        ~TrainSet() {
            delete x;
            delete y;
        }
        NumericMatrix getX() { return (*x)(Range(0,nobs-1),_); }
        IntegerVector getY() { return wrap(*y); }
        void insert( NumericVector obs, int label ) {
            int nrows = x->rows();
            int ncols = x->cols();
            if( nobs == nrows ) {          // enlarge matrix
                nrows = MAX((int)(nrows*1.1),nrows+500);
                NumericMatrix tmpm = NumericMatrix( nrows, ncols );
                for( int i = 0; i < x->rows(); i++ ) //TODO: make this more efficient
                    tmpm(i,_) = (*x)(i,_);
                //tmpm( Range(0,x->rows()-1), Range(0,ncols-1)) = *x;
                *x = tmpm;
            }
            (*x)(nobs,_) = obs+rnorm(ncols,0,noise_var); // add white noise
            y->push_back(label);
            nobs++;
        }
    private:
        NumericMatrix *x;
        std::vector<int> *y;
        int nobs;
        double noise_var;
};

class Pay2Recruit {
    public:
        Pay2Recruit( List settings_ ) : settings(settings_) {
            prob = settings["prob"];
            std::vector<int> run_ids = as<std::vector<int> >(settings["RUN_IDS"]);

            /* Load graph and node attributes */
            g = new Graph( true );
            Rcout << "Graph file:" <<as<std::string>(prob["graph_file"]) << std::endl;
            g->readGraph( as<std::string>(prob["graph_file"]) );
            g->readCommunities3( as<std::string>(prob["attrib_file"]),
                                 as<int>(prob["accept_trait"]),
                                 as<int>(prob["target_trait"]),
                                 as<float>(prob["p_accept_one"]),
                                 as<float>(prob["p_accept_zero"]),
                                 as<float>(prob["p_neighs_accept"]),
                                 as<float>(prob["p_neighs_reject"]) );
            /* Get seeds */
            seed_ids = g->getTargetNodes( run_ids.size() );
            for( int i = 0; i < seed_ids.size(); i++ )
                Rcerr << seed_ids[i] << std::endl;

            /* Read variables */
            noise_var        = as<double>(settings["noise_var"]);
            max_models       = as<double>(settings["max_models"]);
            save_snapshots   = as<bool>(settings["SAVESNAPSHOTS"]);
            basename         = as<std::string>(settings["outfile"]);
            debug            = as<bool>(settings["DEBUG"]);
            parallel_updates = max_models > 1 ? as<bool>(settings["PARALLELMAB"]) : false;

            confidence_param      = as<double>(settings["CONFIDENCE_PARAMETER"]);
            corr_threshold        = as<double>(settings["CORRELATION_THRESHOLD"]);
            corr_bounds_same_sign = as<bool>(settings["CORR_BOUNDS_MUST_HAVE_SAME_SIGN"]);

            /* Set stop condition */
            max_recruited = (int)(g->getN()*0.9);
            std::string criterion_str = as<std::string>(settings["stop_criterion"]);
            if( criterion_str == "budget" )
                stop_criterion = COND_BUDGET;
            else if( criterion_str == "target" )
                stop_criterion = COND_TARGET;
            stop_at = as<int>(settings["stop_at"]);

        }
        ~Pay2Recruit() {
            delete g;
        }

        std::vector<int> modelsInAgreement( std::vector<NodeScoreMap> score, int id, int nmodels ) {
            std::vector<int> model_inds;
            for( int model_ind = 0; model_ind < nmodels; model_ind++ ) {
                double max_score = -std::numeric_limits<double>::max();
                for( NodeScoreMap::iterator it = score[model_ind].begin(); it != score[model_ind].end(); ++it )
                    if( max_score < it->second )
                        max_score = it->second;
                if( score[model_ind][id] == max_score )
                    model_inds.push_back(model_ind);
            }
            return model_inds;
        }

        int getBestNode( NodeScoreMap score ) {
            std::vector<int> best_ids; //TODO: generalize this
            double max_score = -std::numeric_limits<double>::max();

            for( NodeScoreMap::iterator it = score.begin(); it != score.end(); ++it ) {
               if( DEBUG > 1) printf("id %d, score %lf\n", it->first, it->second);
                if( max_score < it->second ) { // 1st is id, 2nd is score
                    max_score = it->second;
                    best_ids.clear();
                }
                if( max_score == it->second )
                    best_ids.push_back(it->first);
            }
            //return *std::min_element(best_ids.begin(),best_ids.end());
            return best_ids[lrand48()%best_ids.size()];
        }


        List run( int run_id );

    private:
        List settings, prob;
        Graph *g;
        std::vector<int> seed_ids;

        static const int COND_TARGET = 0;
        static const int COND_BUDGET = 1;
        static const int NO_ACTION = 0;
        static const int UPDATE    = 1;
        static const int TRAIN     = 2;

        double noise_var, confidence_param, corr_threshold;
        int max_models;
        bool save_snapshots, debug, parallel_updates, corr_bounds_same_sign;
        std::string basename;

        int max_recruited, stop_criterion, stop_at;
};



List Pay2Recruit::run( int run_id ) {
    //srand(run_id); //TODO: uncomment me
    const int ntypes = 2;
    const int target = 1;
    const bool intercept = true;

    /* Save first snapshot */
    if( save_snapshots ) {
        char run_str[10];
        sprintf( run_str, ".r%03d", run_id );
        basename += run_str;

        std::string outfile = basename + ".0000" + ".RData";
        Language call("saveRcpp",List::create(settings),outfile);
        call.eval();
    }

    /* Initialize auxiliar and control variables */
    Graph *f = new Graph( false );
    f->initFeatures( ntypes, target, as<int>(prob["nattribs"]) );
    f->initFeatsBuffer( g->getN()+1, f->nfeatures() ); //TODO: make dynamic indexing

    FeatureEnabler *ftracker = new FeatureEnabler( f->nfeatures(), confidence_param, corr_threshold, corr_bounds_same_sign );
    ModelTracker   *mtracker = new ModelTracker( max_models, ftracker->nfeatures(), as<bool>(settings["sparse"]) );

    MySpMat border_feats( ftracker->nfeatures()+1 );       // border nodes' feats -- +1 for the intercept
    TrainSet trainset( ftracker->nfeatures(), noise_var ); // trainset
    std::vector<NodeScoreMap> score(max_models);           // most up-to-date scores of border nodes
    DynamicIndex border_id2ind;

    std::list<int> recruited;                              // observed ids, to save as snapshot
    int npositive = 0;
    std::vector<int> label_history;                        // observed labels, to save as snapshot

    std::queue<int> to_be_recruited;
    to_be_recruited.push(seed_ids[run_id-1]);
    while( recruited.size() < max_recruited ) {
        int id, ind, model_ind;
        int t = recruited.size();

        if( debug || t%100 == 0 )
            printf("A1. Select border nodes (t=%d, |B|=%lu).\n", t, border_id2ind.size() );
        model_ind = ind = -1;

        if( to_be_recruited.empty() ) {                     // no overload in recruitment
            if( !border_id2ind.empty() ) {                  // best among border nodes
                model_ind = mtracker->getBestModel(t);
                if(debug) printf("A1.1 Update best id, using model_ind %d\n", model_ind);

                id = getBestNode( score[model_ind] );
                //scanf(" %d", &id );
                ind = border_id2ind[id];
            } else {                                        // random jump
                id = g->getRandomNode();
                while( std::find(recruited.begin(),recruited.end(),id) != recruited.end() )
                    id = g->getRandomNode();
                //scanf(" %d", &id );
            }
        } else {                                            // select from queue
            id = to_be_recruited.front();
            to_be_recruited.pop();
            std::map<int,int>::iterator id_it = border_id2ind.find( id );
            if( id_it != border_id2ind.end() ) {
                ind = id_it->second;
            } else {
                // TODO: throw error
            }
        }
        recruited.push_back(id);

        if(debug) printf("A2. Obtain info about node id %d at ind %d\n", id, ind);
        List info = g->getInfo( id );

        int label                 = as<int>(info["label"]);
        std::vector<int> neighs   = as<std::vector<int> >(info["neighbors"]);
        std::vector<int> attribs  = as<std::vector<int> >(info["attributes"]);
        printf("A2. sample #%d : %d , label %d\n", t, id, label);

        // Add node to observed graph f
        std::pair<std::vector<int>,MySpMat> tmp = f->addNode( id, neighs, label, attribs );
        if( tmp.first.size() > 0 ) {
            std::vector<int> touched_ids = tmp.first;
            MySpMat feats                = tmp.second;
            if( DEBUG > 1 ) {
                for( int i = 0; i < touched_ids.size(); i++ )
                    printf( "%d ", touched_ids[i]);
                printf( "\n" );
                feats.print();
            }

            if(debug) printf("A3. Saving info on %lu touched nodes.\n", touched_ids.size() );
            for( int i = 0; i < touched_ids.size(); i++ )
                border_feats.insert( feats[i], border_id2ind[touched_ids[i]] );

            if( !mtracker->anyTrained() ) // use heuristic to initialize scores of new border nodes
                for( std::vector<int>::iterator it = neighs.begin(); it != neighs.end(); it++ ) 
                  if( std::find(recruited.begin(),recruited.end(),*it) == recruited.end() )
                    score[0][*it] += label;
        }


        if( model_ind != -1 ) { // update model statistics
            if( max_models > 1 && parallel_updates ) {
                std::vector<int> agreeing_models = modelsInAgreement( score, id, mtracker->nmodels() );
                mtracker->update( agreeing_models, label );
                //if( debug > 0 && agreeing_models.size() != mtracker->nmodels() ) {
                //    Rcerr << "******************* AGREEING MODELS ******************" << std::endl;
                //    for( int j = 0; j < agreeing_models.size(); j++ )
                //        Rcerr << agreeing_models[j] << " ";
                //    Rcerr << std::endl;
                //}
            } else {
                mtracker->update( model_ind, label );
            }
        }

        if( ind != -1 ) { // not random jump or random choice
            std::vector<int> new_feats;

            if(debug) printf("A4. Update trainset and feature statistics (label %d)\n", label);
            std::vector<double> feat_vec = border_feats[ind]; // load features
            feat_vec.erase(feat_vec.begin()); // remove intercept

            trainset.insert(wrap(feat_vec),label); // update trainset
            bool enabled = ftracker->updateCpp( feat_vec, label, new_feats ); // update feature tracker
            if(debug) if(new_feats.size()) {
                printf( " %lu new features were enabled:", new_feats.size() );
                for( int i = 0; i < new_feats.size(); i++ )
                    printf( " %d", new_feats[i]);
                printf("\n");
            }

            if(debug) printf("A5. Decide on training/updating\n");
            bool *no_longer_perf_sep = new bool [max_models]();
            for( int i = 0; i < max_models; i++ ) {
                double reward = score[i][id];
                no_longer_perf_sep[i] = (label == 0 && reward > -22.5) ||
                    (label == 1 && reward < 22.5); // exp(22.5) \approx \infty
            }

            std::vector<int> update_inds, train_inds;
            std::vector<std::pair<int,int> > from_to;
            if( !enabled ) {
                for( int i = 0; i < max_models; i++ )
                    if( mtracker->isModelValid(i) ) {
                        if( !mtracker->isPerfectlySeparable(i) ) update_inds.push_back(i);
                        else if( no_longer_perf_sep[i] ) train_inds.push_back(i);
                    }
            } else {
                from_to = mtracker->pairBestWorst(t);
                for( std::vector<std::pair<int,int> >::iterator it = from_to.begin(); it != from_to.end(); ++it ) {
                    if( debug ) printf( "Replacement %d + new_feats --> %d\n", it->first, it->second );

                    mtracker->duplicate( it->first, it->second );
                    mtracker->enableFeature(it->second,new_feats);

                    train_inds.push_back(it->second);
                    mtracker->resetStats( it->second, t );
                    if( it->second != it->first ) {
                        if( !mtracker->isPerfectlySeparable(it->first) ) update_inds.push_back(it->first);
                        else if( no_longer_perf_sep[it->first] ) train_inds.push_back(it->first);
                    }
                }
                
            }
            ////////////////////////////////////

            if( !update_inds.empty() ) {
                if(debug) {
                    printf("A6.1. Update inds:" );
                    for( int i = 0; i < update_inds.size(); i++ )
                        printf("%d ", update_inds[i]);
                    printf("\n");
                }
                Language call("lrUpdate",feat_vec,label,mtracker->getModels(),
                        update_inds);
                mtracker->setModels( call.eval() );
            }

            if( !train_inds.empty() ) {
                if(debug) {
                    printf("A6.2 Train inds:" );
                    for( int i = 0; i < train_inds.size(); i++ )
                        printf("%d ", train_inds[i]);
                    printf("\n");
                }
                List new_models(train_inds.size());
                for( int i = 0; i < train_inds.size(); i++ ) {
                    IntegerVector model_feats = mtracker->getModelFeats(train_inds[i]);
                    model_feats.attr("dim") = Dimension(model_feats.size(),1);
                    Language call("lrTrain",trainset.getX(),trainset.getY(),model_feats);
                    List ret = call.eval();
                    mtracker->setPerfectlySeparable( train_inds[i], as<bool>(ret(0)) );
                    new_models(i) = ret(1);
                    mtracker->flagModelFeats(train_inds[i], as<std::vector<int> >(as<List>(ret(1))["flaggedfeats"]));

                    // TODO: get list of flagged features and use clever data structure to
                    // keep track of how long they have to wait until tried again
                }

                if( mtracker->nmodels() > 1 ) {
                    Language call("logistic.dma.set",mtracker->getModels(),new_models,1+IntegerVector(train_inds.begin(),train_inds.end()));
                    mtracker->setModels( call.eval() );
                } else {
                    mtracker->setModels( new_models(0) );
                }
            }

            /* clean-up ind */
            border_id2ind.erase(id);
            for( int i=0; i < max_models; i++)
                score[i].erase(id);
            border_feats.erase(ind);
        }

        if(debug) printf("A7. Update scores\n");
        if( mtracker->anyTrained() ) {
            //NumericMatrix tmp = wrap(Eigen::MatrixXd(as<SpMat>(mtracker->getModels()["theta"])));
            //border_feats.fastMultiply( tmp, border_id2ind.getMap(), score );

            MySpMat coef( mtracker->getModelFeats(), as<List>(mtracker->getModels()["betahat"]), intercept );
            border_feats.fastMultiply( coef, border_id2ind.getMap(), score );
        }


        label_history.push_back(label);
        npositive += label;
        if( save_snapshots && (t+1)%100 == 0 ) {
            if(debug) printf("A8. Save recruited, y, enabled_feats\n");
            char step_str[7];
            sprintf( step_str, ".%04d", t+1 );
            std::string outfile = basename + step_str + ".RData";
            List snapshot = List::create( _["recruited"] = wrap(recruited),
                           _["y"] = wrap(label_history),
                           _["nfeats"] = 10); //FIXME: calculate number of features
            Language call("saveRcpp",snapshot,outfile);
            call.eval();
        }

        if( stop_criterion == COND_BUDGET ) {
            if( recruited.size() == stop_at ) {
                printf("COND_BUDGET=%d reached\n", stop_at);
                break;
            }
        } else if( stop_criterion == COND_TARGET ) {
            if( npositive == stop_at ) {
                printf("COND_TARGET=%d reached\n", stop_at);
                break;
            }
        }
    }
    delete f;
    delete ftracker;
    delete mtracker;
    
    return List::create(_["npositive"]=npositive);
}

RCPP_MODULE(yada){
    using namespace Rcpp ;

    class_<Pay2Recruit>( "Pay2Recruit" )
    .constructor<List>()

    .method( "run", &Pay2Recruit::run , "run p2r dissemination" )
    ;
	                  
    class_<FeatureEnabler>( "FeatureEnabler" )
    .constructor<std::vector<bool>,int,int>()

    .method( "getEnabled",  &FeatureEnabler::getEnabled     , "get number of edges" )
    .method( "nfeatures",  &FeatureEnabler::nfeatures     , "get number of edges" )
    .method( "update",  &FeatureEnabler::update     , "get number of edges" )
    .method( "setAdmissionCriterion", &FeatureEnabler::setAdmissionCriterion, "get number of edges" )
    .method( "clear",    &FeatureEnabler::clear     , "get number of edges" )
    ;
	
    class_<Graph>( "Graph" )
    // expose the default constructor
    .constructor<bool>()

        
    .method( "getE", &Graph::getE     , "get number of edges" )
    .method( "computeFeatures", &Graph::computeFeatures , "add directed edge" )
    .method( "addEdge", &Graph::addEdge , "add directed edge" )
    //.method( "addNode", &Graph::addNode , "add node" )
    .method( "addNode2", &Graph::addNode2 , "add node" )
    .method( "addNode3", &Graph::addNode3 , "add node" )
    .method( "addNode4", &Graph::addNode4 , "add node" )
    .method( "addNode5", &Graph::addNode5 , "add node" )
    .method( "getN", &Graph::getN     , "get number of nodes" )
    .method( "getRandomNode", &Graph::getRandomNode     , "get random node" )
    .method( "initFeatures", &Graph::initFeatures     , "initialize feature names" )
    .method( "nfeatures", &Graph::nfeatures     , "initialize feature names" )
    .method( "getFeatureNames", &Graph::getFeatureNames     , "get feature names" )
    .method( "isFeatureFraction", &Graph::isFeatureFraction     , "returns bool vector indicating whether feature is binary" )
    .method( "getNeighbors", &Graph::getNeighbors     , "get neighbors of a node" )
    .method( "getInNeighbors", &Graph::getInNeighbors     , "get in-neighbors of a node" )
    .method( "getTargetNodes", &Graph::getTargetNodes     , "get a random sample of target nodes" )
    .method( "getLabel", &Graph::getLabel     , "get label of a node" )
    .method( "getAttributes", &Graph::getAttributes     , "get attributes of a node" )
    .method( "getInfo", &Graph::getInfo     , "get attributes of a node" )
    .method( "readGraph", &Graph::readGraph     , "read graph from file" )
    .method( "readCommunities2", &Graph::readCommunities2     , "read communities from file as attributes" )
    .method( "readCommunities3", &Graph::readCommunities3     , "read communities from file as attributes" )
    .method( "writeAttributesToFile", &Graph::writeAttributesToFile     , "write communities to file" )
    .method( "distanceMatrix", &Graph::distanceMatrix     , "write communities to file" )
    .method( "recalculateNodeFeatures", &Graph::recalculateNodeFeatures, "write communities to file" )

    ;
}
